/*
 * Written in 2023 by Márcio Silva <coadde@hyperbola.info>
 *
 * To the extent possible under law, the author(s) have dedicated
 * all copyright and related and neighboring rights to this software
 * to the public domain worldwide.
 * This software is distributed without any warranty.
 *
 * You should have received a copy of the
 * CC0 Public Domain Dedication along with this software.
 * If not, see <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

/* Alpha */
#if defined(__alpha__)
# include "../../../lib/libcrypto/arch/alpha/opensslconf.h"
/* ARM 32 */
#elif defined(__aarch32__) || defined(__arm32__) \
    || defined(__arm__) || defined(__armv7__)
# include "../../../lib/libcrypto/arch/arm/opensslconf.h"
/* ARM 64 */
#elif defined(__aarch64__) || defined(__arm64__)
# include "../../../lib/libcrypto/arch/aarch64/opensslconf.h"
/* HP/PA-RISC */
#elif defined(__hppa__)
# include "../../../lib/libcrypto/arch/hppa/opensslconf.h"
/* Motorola 88000 */
#elif defined(__m88k__) || defined(__luna88k__)
# include "../../../lib/libcrypto/arch/m88k/opensslconf.h"
/* MIPS 64 */
#elif defined(__mips64__) || defined(__loongson__) || defined(__octeon__)
# include "../../../lib/libcrypto/arch/mips64/opensslconf.h"
/* POWER 32 / PowerPC 32 */
#elif defined(__powerpc__) || defined(__macppc__)
# include "../../../lib/libcrypto/arch/powerpc/opensslconf.h"
/* POWER 64 / PowerPC 64 */
#elif defined(__powerpc64__)
# include "../../../lib/libcrypto/arch/powerpc64/opensslconf.h"
/* RiscV 64 */
#elif defined(__riscv64__)
# include "../../../lib/libcrypto/arch/riscv64/opensslconf.h"
/* SH-4 / SuperH v4 */
#elif defined(__sh__) || defined(__sh4__) || defined(__landisk__)
# include "../../../lib/libcrypto/arch/sh/opensslconf.h"
/* SPARC 32 */
#elif defined(__sparc__)
# include "../../../lib/libcrypto/arch/sparc/opensslconf.h"
/* SPARC 64 */
#elif defined(__sparc64__)
# include "../../../lib/libcrypto/arch/sparc64/opensslconf.h"
/* X86 32 / IA32 */
#elif defined(__x86_32__) || defined(__i386__) \
    || defined(__i486__) || defined(__i586__) || defined(__i686__)
# include "../../../lib/libcrypto/arch/i386/opensslconf.h"
/* X86 64 / AMD64 */
#elif defined(__x86_64__) || defined(__amd64__)
# include "../../../lib/libcrypto/arch/amd64/opensslconf.h"
#endif
