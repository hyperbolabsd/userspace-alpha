#	$OpenBSD: Makefile,v 1.4 2014/12/23 19:32:16 pascal Exp $

PROGDIR=	${.CURDIR}/../../../usr.sbin/traceroute

# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * CFLAGS
# * CRTBEGIN
# * CRTEND
# * LDADD
# * LIBCRT0
# * STATIC
# * LDSTATIC
#
# Start of modifications made by Hyperbola Project

# Use system headers from source
CFLAGS+=-I${.CURDIR}/../../../build/include

# Dependency libraries
#LDADD+=${.CURDIR}/../../../lib/libc/obj/libc.a
LDADD+=${.CURDIR}/../../../lib/libc/obj/libc.so.96.1
#LDADD+=${.CURDIR}/../../../lib/libevent/obj/libevent.a
LDADD+=${.CURDIR}/../../../lib/libevent/obj/libevent.so.4.1

# Set "ld.so" path
LDADD+=-Wl,-dynamic-linker,/libexec/ld.so

# Fix this warning: cannot find entry symbol _start
LIBCRT0=${.CURDIR}/../../../lib/csu/obj/crt0.o
CRTBEGIN=${.CURDIR}/../../../lib/csu/obj/crtbegin.o
CRTEND=${.CURDIR}/../../../lib/csu/obj/crtend.o
LDADD+=${.CURDIR}/../../../lib/csu/obj/crt0.o
LDADD+=${.CURDIR}/../../../lib/csu/obj/crtbegin.o

# Disable static build
STATIC=
#LDSTATIC=	${STATIC}
LDSTATIC=

# End of modifications made by Hyperbola Project

CFLAGS+=	-I${PROGDIR}
NOMAN=		yes

BINDIR=		/var/www/bin
LINKS=		${BINDIR}/traceroute ${BINDIR}/traceroute6

# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * .include (commented)
# * PROG (copied from sbin/traceroute)
# * SRCS (copied from sbin/traceroute)
# * CFLAGS (copied from sbin/traceroute)
# * MAN (copied from sbin/traceroute)
# * LINK (copied from sbin/traceroute)
# * BINOWN (copied from sbin/traceroute)
# * BINMODE (copied from sbin/traceroute)
# * .include (copied from sbin/traceroute)
#
# Start of modifications made by Hyperbola Project
#.include "${PROGDIR}/Makefile"

PROG=	traceroute

SRCS=	traceroute.c worker.c

CFLAGS+= -Wall -I${.CURDIR}
CFLAGS+= -Wstrict-prototypes -Wmissing-prototypes
CFLAGS+= -Wmissing-declarations
CFLAGS+= -Wshadow -Wpointer-arith -Wcast-qual

MAN=	traceroute.8

LINKS=	${BINDIR}/traceroute ${BINDIR}/traceroute6
BINOWN=	root
BINMODE=4555

.include <bsd.prog.mk>

# End of modifications made by Hyperbola Project

BINMODE=	000

.PATH:		${PROGDIR}
