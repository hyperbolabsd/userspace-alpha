#	$OpenBSD: Makefile,v 1.11 2019/11/04 15:25:54 jsing Exp $

.include <bsd.own.mk>

PROG=	openssl

# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * CFLAGS
# * DPADD
# * LDADD
# * STATIC
#
# Start of modifications made by Hyperbola Project
#LDADD=	-lssl -lcrypto
#DPADD=	${LIBSSL} ${LIBCRYPTO}

# Use system headers from source
CFLAGS+=-I${.CURDIR}/../../build/include

# Dependency libraries
#LDADD+=${.CURDIR}/../../lib/libc/obj/libc.a
LDADD+=${.CURDIR}/../../lib/libc/obj/libc.so.96.1
#LDADD+=${.CURDIR}/../../lib/libcrypto/obj/libcrypto.a
LDADD+=${.CURDIR}/../../lib/libcrypto/obj/libcrypto.so.47.0
#LDADD+=${.CURDIR}/../../lib/libssl/obj/libssl.a
LDADD+=${.CURDIR}/../../lib/libssl/obj/libssl.so.50.0

# Set "ld.so" path
LDADD+=-Wl,-dynamic-linker,/libexec/ld.so

# Fix this warning: cannot find entry symbol _start
LDADD+=${.CURDIR}/../../lib/csu/obj/crt0.o
LDADD+=${.CURDIR}/../../lib/csu/obj/crtbegin.o

# Disable static build
STATIC=

# End of modifications made by Hyperbola Project

CFLAGS+= -Wall
CFLAGS+= -Wformat
CFLAGS+= -Wformat-security
CFLAGS+= -Wimplicit
CFLAGS+= -Wreturn-type
#CFLAGS+= -Wshadow
CFLAGS+= -Wtrigraphs
CFLAGS+= -Wuninitialized
CFLAGS+= -Wunused
.if ${COMPILER_VERSION:tl} == "clang"
CFLAGS+= -Werror
.endif
CFLAGS+= -DLIBRESSL_INTERNAL

SRCS=	apps.c apps_posix.c asn1pars.c ca.c certhash.c ciphers.c cms.c crl.c \
	crl2p7.c dgst.c dh.c dhparam.c dsa.c dsaparam.c ec.c ecparam.c enc.c \
	errstr.c gendh.c gendsa.c genpkey.c genrsa.c nseq.c ocsp.c \
	openssl.c passwd.c pkcs12.c pkcs7.c pkcs8.c pkey.c pkeyparam.c \
	pkeyutl.c prime.c rand.c req.c rsa.c rsautl.c s_cb.c s_client.c \
	s_server.c s_socket.c s_time.c sess_id.c smime.c speed.c spkac.c ts.c \
	verify.c version.c x509.c

.include <bsd.prog.mk>
