#	$OpenBSD: Makefile,v 1.11 2019/12/13 19:09:10 djm Exp $

.PATH:		${.CURDIR}/..

SRCS=	ssh-pkcs11-helper.c ${SRCS_PKCS11}
SRCS+=	atomicio.c compat.c fatal.c readpass.c
SRCS+=	${SRCS_KEY} ${SRCS_UTL} ${SRCS_BASE} ${SRCS_SK_CLIENT}

PROG=	ssh-pkcs11-helper

BINDIR=	/usr/libexec
MAN=	ssh-pkcs11-helper.8

# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * CFLAGS
# * CRTBEGIN
# * CRTEND
# * DPADD
# * LDADD
# * LIBCRT0
# * STATIC
#
# Start of modifications made by Hyperbola Project

# Use system headers from source
CFLAGS+=-I${.CURDIR}/../../../build/include

# Set "ld.so" path
LDADD+=-Wl,-dynamic-linker,/libexec/ld.so

# Fix this warning: cannot find entry symbol _start
LIBCRT0=${.CURDIR}/../../../lib/csu/obj/crt0.o
CRTBEGIN=${.CURDIR}/../../../lib/csu/obj/crtbegin.o
CRTEND=${.CURDIR}/../../../lib/csu/obj/crtend.o
LDADD+=${.CURDIR}/../../../lib/csu/obj/crt0.o
LDADD+=${.CURDIR}/../../../lib/csu/obj/crtbegin.o

# Disable static build
STATIC=

.include <bsd.prog.mk>

#LDADD+=	-lcrypto -lutil
#DPADD+=	${LIBCRYPTO} ${LIBUTIL}

# Dependency libraries
#LDADD+=${.CURDIR}/../../../lib/libc/obj/libc.a
LDADD+=${.CURDIR}/../../../lib/libc/obj/libc.so.96.1

# Dependency libraries
#LDADD+=${.CURDIR}/../../../lib/libcrypto/obj/libcrypto.a
LDADD+=${.CURDIR}/../../../lib/libcrypto/obj/libcrypto.so.47.0
#LDADD+=${.CURDIR}/../../../lib/libutil/obj/libutil.a
LDADD+=${.CURDIR}/../../../lib/libutil/obj/libutil.so.15.1

# End of modifications made by Hyperbola Project
