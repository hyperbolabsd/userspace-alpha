#	$OpenBSD: Makefile,v 1.81 2020/01/25 23:02:14 djm Exp $

.PATH:		${.CURDIR}/..

SRCS=	ssh.c readconf.c clientloop.c sshtty.c sshconnect.c sshconnect2.c mux.c
SRCS+=	authfd.c compat.c dns.c fatal.c hostfile.c readpass.c utf8.c
SRCS+=	${SRCS_BASE} ${SRCS_KEX} ${SRCS_KEXC} ${SRCS_KEY} ${SRCS_KEYP} \
	${SRCS_KRL} ${SRCS_PROT} ${SRCS_PKT} ${SRCS_UTL} ${SRCS_PKCS11} \
	${SRCS_SK_CLIENT}

PROG=	ssh

BINDIR=	/usr/bin
MAN=	ssh.1 ssh_config.5

.include <bsd.own.mk>

KERBEROS5=no

.if (${KERBEROS5:tl} == "yes")
CFLAGS+= -DKRB5 -I${DESTDIR}/usr/include/kerberosV -DGSSAPI
.endif # KERBEROS5

# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * CFLAGS
# * CRTBEGIN
# * CRTEND
# * DPADD
# * LDADD
# * LIBCRT0
# * STATIC
#
# Start of modifications made by Hyperbola Project

# Use system headers from source
CFLAGS+=-I${.CURDIR}/../../../build/include

# Set "ld.so" path
LDADD+=-Wl,-dynamic-linker,/libexec/ld.so

# Fix this warning: cannot find entry symbol _start
LIBCRT0=${.CURDIR}/../../../lib/csu/obj/crt0.o
CRTBEGIN=${.CURDIR}/../../../lib/csu/obj/crtbegin.o
CRTEND=${.CURDIR}/../../../lib/csu/obj/crtend.o
LDADD+=${.CURDIR}/../../../lib/csu/obj/crt0.o
LDADD+=${.CURDIR}/../../../lib/csu/obj/crtbegin.o

# Disable static build
STATIC=

.include <bsd.prog.mk>

# Dependency libraries
#LDADD+=${.CURDIR}/../../../lib/libc/obj/libc.a
LDADD+=${.CURDIR}/../../../lib/libc/obj/libc.so.96.1

.if (${KERBEROS5:tl} == "yes")
DPADD+=  ${LIBGSSAPI} ${LIBKRB5}
LDADD+=  -lgssapi -lkrb5 -lasn1
LDADD+=  -lwind -lroken -lcom_err -lpthread -lheimbase
.endif # KERBEROS5

.if (${OPENSSL:tl} == "yes")
#LDADD+=	-lcrypto
#DPADD+=	${LIBCRYPTO}

# Dependency libraries
#LDADD+=${.CURDIR}/../../../lib/libcrypto/obj/libcrypto.a
LDADD+=${.CURDIR}/../../../lib/libcrypto/obj/libcrypto.so.47.0
.endif

#LDADD+=	-lutil
#DPADD+=	${LIBUTIL}

# Dependency libraries
#LDADD+=${.CURDIR}/../../../lib/libutil/obj/libutil.a
LDADD+=${.CURDIR}/../../../lib/libutil/obj/libutil.so.15.1

.if (${ZLIB:tl} == "yes")
#LDADD+=	-lz
#DPADD+=	${LIBZ}

# Dependency libraries
#LDADD+=${.CURDIR}/../../../lib/libz/obj/libz.a
LDADD+=${.CURDIR}/../../../lib/libz/obj/libz.so.6.0
.endif

# End of modifications made by Hyperbola Project
