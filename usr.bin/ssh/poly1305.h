/* $OpenBSD: poly1305.h,v 1.4 2014/05/02 03:27:54 djm Exp $ */

/*
 * Public Domain poly1305 from Andrew Moon
 * poly1305-donna-unrolled.c
 * from https://github.com/floodyberry/poly1305-donna
 *
 * Modifications to support HyperbolaBSD:
 * Written in 2022 by Hyperbola Project
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef POLY1305_H
#define POLY1305_H

#include <sys/types.h>

#define POLY1305_KEYLEN		32
#define POLY1305_TAGLEN		16

void poly1305_auth(u_char out[POLY1305_TAGLEN], const u_char *m, size_t inlen,
    const u_char key[POLY1305_KEYLEN]);

#endif	/* POLY1305_H */
