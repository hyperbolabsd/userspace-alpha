# $OpenBSD: Makefile,v 1.35 2019/07/18 05:57:48 lum Exp $

PROG=	mg

# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * CFLAGS
# * DPADD
# * LDADD
# * STATIC
#
# Start of modifications made by Hyperbola Project
#LDADD+=	-lcurses -lutil
#DPADD+=	${LIBCURSES} ${LIBUTIL}

# Use system headers from source
CFLAGS+=-I${.CURDIR}/../../build/include

# Dependency libraries
#LDADD+=${.CURDIR}/../../lib/libc/obj/libc.a
LDADD+=${.CURDIR}/../../lib/libc/obj/libc.so.96.1
#LDADD+=${.CURDIR}/../../lib/libcurses/obj/libcurses.a
LDADD+=${.CURDIR}/../../lib/libcurses/obj/libcurses.so.14.0
#LDADD+=${.CURDIR}/../../lib/libutil/obj/libutil.a
LDADD+=${.CURDIR}/../../lib/libutil/obj/libutil.so.15.1

# Set "ld.so" path
LDADD+=-Wl,-dynamic-linker,/libexec/ld.so

# Fix this warning: cannot find entry symbol _start
LDADD+=${.CURDIR}/../../lib/csu/obj/crt0.o
LDADD+=${.CURDIR}/../../lib/csu/obj/crtbegin.o

# Disable static build
STATIC=

# End of modifications made by Hyperbola Project

# (Common) compile-time options:
#
#	REGEX		-- create regular expression functions.
#	STARTUPFILE	-- look for and handle initialization file.
#	MGLOG		-- debug mg internals to a log file.
#
CFLAGS+=-Wall -DREGEX

SRCS=	autoexec.c basic.c bell.c buffer.c cinfo.c dir.c display.c \
	echo.c extend.c file.c fileio.c funmap.c help.c kbd.c keymap.c \
	line.c macro.c main.c match.c modes.c paragraph.c \
	re_search.c region.c search.c spawn.c tty.c ttyio.c ttykbd.c \
	undo.c util.c version.c window.c word.c yank.c

#
# More or less standalone extensions.
#
SRCS+=	cmode.c cscope.c dired.c grep.c interpreter.c tags.c

#
# -DMGLOG source file.
#
#SRCS+=	log.c

afterinstall:
	${INSTALL} -d -o root -g wheel ${DESTDIR}${DOCDIR}/mg
	${INSTALL} ${INSTALL_COPY} -o ${DOCOWN} -g ${DOCGRP} -m ${DOCMODE} \
	    ${.CURDIR}/tutorial ${DESTDIR}${DOCDIR}/mg

.include <bsd.prog.mk>
