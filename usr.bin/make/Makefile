#	$OpenBSD: Makefile,v 1.64 2020/01/13 15:41:53 espie Exp $

PROG=	make
CFLAGS+= -I${.OBJDIR} -I${.CURDIR}
HOSTCFLAGS+= -I${.OBJDIR} -I${.CURDIR}
CDIAGFLAGS=-Wall -W -Wno-char-subscripts -Wstrict-prototypes -pedantic \
	-Wmissing-prototypes -Wdeclaration-after-statement -std=c99

CDEFS+=-DHAS_PATHS_H
CDEFS+=-DHAS_EXTENDED_GETCWD
#CDEFS+=-DHAS_STATS

# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * CFLAGS
# * DPADD
# * LDADD
# * STATIC
#
# Start of modifications made by Hyperbola Project
#DPADD += ${LIBUTIL}
#LDADD += -lutil

# Use system headers from source
CFLAGS+=-I${.CURDIR}/../../build/include
HOSTCFLAGS+=-I${.CURDIR}/../../build/include

# Dependency libraries
LDADD+=${.CURDIR}/../../lib/libc/obj/libc.a
#LDADD+=${.CURDIR}/../../lib/libc/obj/libc.so.96.1
LDADD+=${.CURDIR}/../../lib/libutil/obj/libutil.a
#LDADD+=${.CURDIR}/../../lib/libutil/obj/libutil.so.15.1

# Set "ld.so" path
LDADD+=-Wl,-dynamic-linker,/libexec/ld.so

# Fix this warning: cannot find entry symbol _start
LDADD+=${.CURDIR}/../../lib/csu/obj/crt0.o
LDADD+=${.CURDIR}/../../lib/csu/obj/crtbegin.o

# Disable static build
STATIC=

# End of modifications made by Hyperbola Project

CFLAGS+=${CDEFS}
HOSTCFLAGS+=${CDEFS}

SRCS=	arch.c buf.c cmd_exec.c compat.c cond.c dir.c direxpand.c dump.c \
	engine.c enginechoice.c error.c expandchildren.c \
	for.c init.c job.c lowparse.c main.c make.c memory.c parse.c \
	parsevar.c str.c stats.c suff.c targ.c targequiv.c timestamp.c \
	var.c varmodifiers.c varname.c

.include "${.CURDIR}/lst.lib/Makefile.inc"

CLEANFILES+=generate generate.o regress.o check

CLEANFILES+= varhashconsts.h condhashconsts.h nodehashconsts.h

# may need tweaking if you add variable synonyms or change the hash function
MAGICVARSLOTS=82
MAGICCONDSLOTS=65

varhashconsts.h: generate
	${.OBJDIR}/generate 1 ${MAGICVARSLOTS} >$@.tmp && mv $@.tmp $@

condhashconsts.h: generate
	${.OBJDIR}/generate 2 ${MAGICCONDSLOTS} >$@.tmp && mv $@.tmp $@

nodehashconsts.h: generate
	${.OBJDIR}/generate 3 0 >$@.tmp && mv $@.tmp $@

generate: generate.c stats.c memory.c ${DPADD}
	${HOSTCC} ${HOSTCFLAGS} ${LDSTATIC} -o ${.TARGET} ${.ALLSRC} ${LDFLAGS} ${LDADD}

CHECKOBJS = regress.o str.o memory.o buf.o

check: ${CHECKOBJS} ${DPADD}
	${CC} -o ${.TARGET} ${CFLAGS} ${CHECKOBJS} ${LDADD}

regress: check
	${.OBJDIR}/check

var.o: varhashconsts.h
cond.o: condhashconsts.h
targ.o parse.o: nodehashconsts.h

.PHONY:		regress

.include <bsd.prog.mk>
