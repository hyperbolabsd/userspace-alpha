# $OpenBSD: Makefile,v 1.18 2018/01/15 19:45:51 brynet Exp $

PROG=   file
SRCS=   file.c magic-dump.c magic-load.c magic-test.c magic-common.c \
	text.c xmalloc.c
MAN=	file.1 magic.5

# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * CFLAGS
# * DPADD
# * LDADD
# * STATIC
#
# Start of modifications made by Hyperbola Project
#LDADD=	-lutil
#DPADD=	${LIBUTIL}

# Use system headers from source
CFLAGS+=-I${.CURDIR}/../../build/include

# Dependency libraries
#LDADD+=${.CURDIR}/../../lib/libc/obj/libc.a
LDADD+=${.CURDIR}/../../lib/libc/obj/libc.so.96.1
#LDADD+=${.CURDIR}/../../lib/libutil/obj/libutil.a
LDADD+=${.CURDIR}/../../lib/libutil/obj/libutil.so.15.1

# Set "ld.so" path
LDADD+=-Wl,-dynamic-linker,/libexec/ld.so

# Fix this warning: cannot find entry symbol _start
LDADD+=${.CURDIR}/../../lib/csu/obj/crt0.o
LDADD+=${.CURDIR}/../../lib/csu/obj/crtbegin.o

# Disable static build
STATIC=

# End of modifications made by Hyperbola Project

CDIAGFLAGS+= -Wno-long-long -Wall -W -Wnested-externs -Wformat=2
CDIAGFLAGS+= -Wmissing-prototypes -Wstrict-prototypes -Wmissing-declarations
CDIAGFLAGS+= -Wwrite-strings -Wshadow -Wpointer-arith -Wsign-compare
CDIAGFLAGS+= -Wundef -Wbad-function-cast -Winline -Wcast-align

MAGIC=		/etc/magic
MAGICOWN=	root
MAGICGRP=	bin
MAGICMODE=	444

CLEANFILES+=	magic post-magic

MAG1=		$(.CURDIR)/magdir/Header \
		$(.CURDIR)/magdir/Localstuff \
		$(.CURDIR)/magdir/HyperbolaBSD
MAGFILES=	$(.CURDIR)/magdir/[0-9a-z]*

post-magic: $(MAGFILES)
	for i in ${.ALLSRC:N*.orig}; do \
		echo $$i; \
	done|sort|xargs -n 1024 cat >$(.TARGET)

magic: $(MAG1) post-magic
	cat ${MAG1} post-magic >$(.TARGET)

afterinstall:
	${INSTALL} ${INSTALL_COPY} -o $(MAGICOWN) -g $(MAGICGRP) \
		-m $(MAGICMODE) magic $(DESTDIR)$(MAGIC)

all: file magic

.include <bsd.prog.mk>
