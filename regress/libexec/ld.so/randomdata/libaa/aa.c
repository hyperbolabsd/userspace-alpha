/* $OpenBSD: aa.c,v 1.3 2017/08/01 13:05:55 deraadt Exp $ */

/*
 * Public Domain
 *
 * Modifications to support HyperbolaBSD:
 * Written in 2023 by Hyperbola Project
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <sys/types.h>

int64_t aavalue __attribute__((section(".randomdata")));

int64_t
getaavalue()
{
	return aavalue;
}
