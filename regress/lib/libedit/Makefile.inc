#	$OpenBSD: Makefile.inc,v 1.4 2019/05/10 15:36:19 bluhm Exp $

TOPSRC=		${.CURDIR}/../../../../lib/libedit
# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * TOPOBJ
#
# Start of modifications made by Hyperbola Project
TOPOBJ!=	${MAKE} -C ${TOPSRC} -V .OBJDIR
# End of modifications made by Hyperbola Project
CFLAGS+=	-I${TOPSRC} -I${TOPOBJ}

${PROG} ${PROGS}: ${TOPOBJ}/common.h

${TOPOBJ}/common.h:
	${MAKE} -C ${TOPSRC} generated
