# Written in 2023 by Márcio Silva <coadde@hyperbola.info>
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.

OBJS = include/obj \
       lib/csu/obj lib/libagentx/obj \
       lib/libarch/alpha/obj lib/libarch/amd64/obj lib/libarch/arm/obj \
       lib/libarch/i386/obj lib/libarch/mips64/obj \
       lib/libcbor/obj lib/libc/obj lib/libcrypto/obj lib/libcsi/obj \
       lib/libcurses/obj lib/libedit/obj lib/libelf/obj lib/libevent/obj \
       lib/libexpat/obj lib/libfido2/obj lib/libform/obj lib/libfuse/obj \
       lib/libkeynote/obj lib/libkvm/obj lib/libl/obj lib/libmenu/obj \
       lib/libm/obj lib/libossaudio/obj lib/libpanel/obj lib/libpcap/obj \
       lib/libradius/obj lib/librpcsvc/obj lib/librthread/obj \
       lib/libskey/obj lib/libsndio/obj lib/libssl/obj lib/libtls/obj \
       lib/libusbhid/obj lib/libutil/obj lib/liby/obj lib/libz/obj \
       libexec/comsat/obj libexec/fingerd/obj libexec/ftpd/obj \
       libexec/getty/obj \
       libexec/ld.so/ldconfig/obj libexec/ld.so/ldd/obj libexec/ld.so/obj \
       libexec/lockspool/obj libexec/login_chpass/obj \
       libexec/login_lchpass/obj libexec/login_ldap/obj \
       libexec/login_passwd/obj libexec/login_radius/obj \
       libexec/login_reject/obj libexec/login_skey/obj \
       libexec/login_token/obj libexec/login_yubikey/obj \
       libexec/mail.local/obj libexec/reorder_kernel/obj \
       libexec/rpc.rquotad/obj libexec/rpc.rstatd/obj \
       libexec/rpc.rusersd/obj libexec/rpc.rwalld/obj libexec/security/obj \
       libexec/spamd/obj libexec/spamd-setup/obj libexec/spamlogd/obj \
       libexec/talkd/obj libexec/tradcpp/obj \
       boot/arch/amd64/stand/biosboot/obj boot/arch/amd64/stand/boot/obj \
       boot/arch/amd64/stand/cdboot/obj boot/arch/amd64/stand/cdbr/obj \
       boot/arch/amd64/stand/efiboot/bootia32/obj \
       boot/arch/amd64/stand/efiboot/bootx64/obj \
       boot/arch/amd64/stand/fdboot/obj boot/arch/amd64/stand/mbr/obj \
       boot/arch/amd64/stand/pxeboot/obj \
       bin/cat/obj bin/chio/obj bin/chmod/obj bin/cp/obj bin/csh/obj \
       bin/date/obj bin/dd/obj bin/df/obj bin/domainname/obj bin/echo/obj \
       bin/ed/obj bin/expr/obj bin/hostname/obj bin/kill/obj bin/ksh/obj \
       bin/ln/obj bin/ls/obj bin/md5/obj bin/mkdir/obj bin/mt/obj bin/mv/obj \
       bin/pax/obj bin/ps/obj bin/pwd/obj bin/rmdir/obj bin/rm/obj \
       bin/sleep/obj bin/stty/obj bin/sync/obj bin/test/obj \
       sbin/atactl/obj sbin/badsect/obj sbin/bioctl/obj sbin/clri/obj \
       sbin/dhclient/obj sbin/dhcpleased/obj sbin/disklabel/obj \
       sbin/dmesg/obj sbin/dumpfs/obj sbin/dump/obj sbin/fdisk/obj \
       sbin/fsck_ext2fs/obj sbin/fsck_ffs/obj sbin/fsck_msdos/obj \
       sbin/fsck/obj sbin/fsdb/obj sbin/fsirand/obj sbin/growfs/obj \
       sbin/ifconfig/obj sbin/iked/obj sbin/init/obj sbin/ipsecctl/obj \
       sbin/isakmpd/obj sbin/kbd/obj sbin/ldattach/obj sbin/mknod/obj \
       sbin/mount_cd9660/obj sbin/mountd/obj sbin/mount_ext2fs/obj \
       sbin/mount_ffs/obj sbin/mount_msdos/obj sbin/mount_nfs/obj \
       sbin/mount_ntfs/obj sbin/mount/obj sbin/mount_tmpfs/obj \
       sbin/mount_udf/obj sbin/mount_vnd/obj sbin/ncheck_ffs/obj \
       sbin/newfs_ext2fs/obj sbin/newfs_msdos/obj sbin/newfs/obj \
       sbin/nfsd/obj sbin/nologin/obj sbin/pdisk/obj sbin/pfctl/obj \
       sbin/pflogd/obj sbin/ping/obj sbin/quotacheck/obj sbin/reboot/obj \
       sbin/resolvd/obj sbin/restore/obj sbin/route/obj sbin/savecore/obj \
       sbin/scan_ffs/obj sbin/scsi/obj sbin/shutdown/obj sbin/slaacd/obj \
       sbin/swapctl/obj sbin/sysctl/obj sbin/ttyflags/obj sbin/tunefs/obj \
       sbin/umount/obj sbin/unwind/obj sbin/vnconfig/obj sbin/wsconsctl/obj \
       usr.bin/apply/obj usr.bin/arch/obj usr.bin/at/obj usr.bin/aucat/obj \
       usr.bin/audioctl/obj usr.bin/awk/obj usr.bin/banner/obj \
       usr.bin/basename/obj usr.bin/bc/obj \
       usr.bin/bgplg/bgpctl/obj usr.bin/bgplg/bgplg/obj \
       usr.bin/bgplg/bgplgsh/obj usr.bin/bgplg/ping/obj \
       usr.bin/bgplg/traceroute/obj \
       usr.bin/biff/obj usr.bin/calendar/obj usr.bin/cal/obj \
       usr.bin/cap_mkdb/obj usr.bin/cdio/obj usr.bin/chpass/obj \
       usr.bin/cmp/obj usr.bin/col/obj usr.bin/colrm/obj usr.bin/column/obj \
       usr.bin/comm/obj usr.bin/compress/obj usr.bin/cpp/obj \
       usr.bin/crontab/obj usr.bin/csplit/obj usr.bin/ctags/obj \
       usr.bin/ctfconv/obj usr.bin/ctfdump/obj usr.bin/cu/obj \
       usr.bin/cut/obj usr.bin/cvs/obj usr.bin/dc/obj usr.bin/deroff/obj \
       usr.bin/diff3/obj usr.bin/diff/obj usr.bin/dig/obj \
       usr.bin/dirname/obj usr.bin/doas/obj usr.bin/du/obj \
       usr.bin/encrypt/obj usr.bin/env/obj usr.bin/expand/obj \
       usr.bin/false/obj usr.bin/fgen/obj usr.bin/file/obj usr.bin/find/obj \
       usr.bin/finger/obj usr.bin/fmt/obj usr.bin/fold/obj usr.bin/from/obj \
       usr.bin/fstat/obj usr.bin/ftp/obj usr.bin/gencat/obj \
       usr.bin/getcap/obj usr.bin/getconf/obj usr.bin/getent/obj \
       usr.bin/getopt/obj usr.bin/gprof/obj usr.bin/grep/obj \
       usr.bin/head/obj usr.bin/hexdump/obj usr.bin/htpasswd/obj \
       usr.bin/id/obj usr.bin/indent/obj usr.bin/infocmp/obj \
       usr.bin/ipcrm/obj usr.bin/ipcs/obj usr.bin/join/obj usr.bin/jot/obj \
       usr.bin/kdump/obj usr.bin/keynote/obj usr.bin/kstat/obj \
       usr.bin/ktrace/obj usr.bin/lam/obj usr.bin/lastcomm/obj \
       usr.bin/last/obj usr.bin/ldap/obj usr.bin/leave/obj \
       usr.bin/less/lesskey/obj usr.bin/less/less/obj \
       usr.bin/lex/obj usr.bin/libtool/obj usr.bin/lndir/obj \
       usr.bin/locale/obj \
       usr.bin/locate/bigram/obj usr.bin/locate/code/obj \
       usr.bin/locate/locate/obj \
       usr.bin/lock/obj usr.bin/logger/obj usr.bin/login/obj \
       usr.bin/logname/obj usr.bin/look/obj usr.bin/lorder/obj \
       usr.bin/m4/obj usr.bin/mail/obj usr.bin/make/obj usr.bin/mandoc/obj \
       usr.bin/mesg/obj usr.bin/mg/obj usr.bin/midicat/obj \
       usr.bin/mixerctl/obj usr.bin/mkdep/obj usr.bin/mklocale/obj \
       usr.bin/mktemp/obj usr.bin/nc/obj usr.bin/netstat/obj \
       usr.bin/newsyslog/obj usr.bin/nfsstat/obj usr.bin/nice/obj \
       usr.bin/nl/obj usr.bin/nm/obj usr.bin/nohup/obj usr.bin/openssl/obj \
       usr.bin/pagesize/obj usr.bin/passwd/obj usr.bin/paste/obj \
       usr.bin/patch/obj usr.bin/pctr/obj usr.bin/pkg-config/obj \
       usr.bin/pkill/obj usr.bin/printenv/obj usr.bin/printf/obj \
       usr.bin/pr/obj usr.bin/quota/obj usr.bin/radioctl/obj usr.bin/rcs/obj \
       usr.bin/rdistd/obj usr.bin/rdist/obj usr.bin/readlink/obj \
       usr.bin/renice/obj usr.bin/rev/obj usr.bin/rpcgen/obj \
       usr.bin/rpcinfo/obj usr.bin/rs/obj usr.bin/rsync/obj usr.bin/rup/obj \
       usr.bin/rusers/obj usr.bin/rwall/obj usr.bin/script/obj \
       usr.bin/sdiff/obj usr.bin/sed/obj usr.bin/sendbug/obj \
       usr.bin/shar/obj usr.bin/showmount/obj usr.bin/signify/obj \
       usr.bin/skeyaudit/obj usr.bin/skeyinfo/obj usr.bin/skeyinit/obj \
       usr.bin/skey/obj usr.bin/sndioctl/obj usr.bin/sndiod/obj \
       usr.bin/snmp/obj usr.bin/sort/obj usr.bin/spell/obj usr.bin/split/obj \
       usr.bin/ssh/obj usr.bin/ssh/scp/obj usr.bin/ssh/sftp/obj \
       usr.bin/ssh/sftp-server/obj usr.bin/ssh/ssh-add/obj \
       usr.bin/ssh/ssh-agent/obj usr.bin/ssh/sshd/obj \
       usr.bin/ssh/ssh-keygen/obj usr.bin/ssh/ssh-keyscan/obj \
       usr.bin/ssh/ssh-keysign/obj usr.bin/ssh/ssh/obj \
       usr.bin/ssh/ssh-pkcs11-helper/obj usr.bin/ssh/ssh-sk-helper/obj \
       usr.bin/stat/obj usr.bin/su/obj usr.bin/systat/obj usr.bin/tail/obj \
       usr.bin/talk/obj usr.bin/tcpbench/obj usr.bin/tee/obj \
       usr.bin/telnet/obj usr.bin/tftp/obj usr.bin/tic/obj usr.bin/time/obj \
       usr.bin/timeout/obj usr.bin/tmux/obj usr.bin/top/obj \
       usr.bin/touch/obj usr.bin/tput/obj usr.bin/tr/obj usr.bin/true/obj \
       usr.bin/tset/obj usr.bin/tsort/obj usr.bin/tty/obj usr.bin/ul/obj \
       usr.bin/uname/obj usr.bin/unexpand/obj usr.bin/unifdef/obj \
       usr.bin/uniq/obj usr.bin/units/obj usr.bin/unvis/obj \
       usr.bin/usbhidaction/obj usr.bin/usbhidctl/obj usr.bin/users/obj \
       usr.bin/uudecode/obj usr.bin/uuencode/obj usr.bin/vacation/obj \
       usr.bin/vi/build/obj usr.bin/vis/obj usr.bin/vmstat/obj \
       usr.bin/wall/obj usr.bin/wc/obj usr.bin/what/obj usr.bin/which/obj \
       usr.bin/whois/obj usr.bin/who/obj usr.bin/w/obj usr.bin/write/obj \
       usr.bin/x99token/obj usr.bin/xargs/obj usr.bin/xinstall/obj \
       usr.bin/yacc/obj usr.bin/yes/obj usr.bin/ypcat/obj \
       usr.bin/ypmatch/obj usr.bin/ypwhich/obj \
       usr.sbin/accton/obj usr.sbin/acme-client/obj usr.sbin/ac/obj \
       usr.sbin/acpidump/obj usr.sbin/adduser/obj \
       usr.sbin/amd/amd/obj usr.sbin/amd/amq/obj usr.sbin/amd/doc/obj \
       usr.sbin/apmd/obj usr.sbin/apm/obj usr.sbin/arp/obj \
       usr.sbin/authpf/obj usr.sbin/bgpctl/obj usr.sbin/bgpd/obj \
       usr.sbin/btrace/obj usr.sbin/chroot/obj usr.sbin/config/obj \
       usr.sbin/cron/obj usr.sbin/crunchgen/obj usr.sbin/dev_mkdb/obj \
       usr.sbin/dhcpd/obj usr.sbin/dhcpleasectl/obj usr.sbin/dhcrelay6/obj \
       usr.sbin/dhcrelay/obj usr.sbin/dvmrpctl/obj usr.sbin/dvmrpd/obj \
       usr.sbin/edquota/obj usr.sbin/eeprom/obj usr.sbin/eigrpctl/obj \
       usr.sbin/eigrpd/obj usr.sbin/fdformat/obj usr.sbin/ftp-proxy/obj \
       usr.sbin/gpioctl/obj usr.sbin/hostapd/obj usr.sbin/hostctl/obj \
       usr.sbin/hotplugd/obj usr.sbin/httpd/obj usr.sbin/identd/obj \
       usr.sbin/ifstated/obj usr.sbin/ikectl/obj usr.sbin/inetd/obj \
       usr.sbin/installboot/obj usr.sbin/iostat/obj usr.sbin/iscsictl/obj \
       usr.sbin/iscsid/obj usr.sbin/kgmon/obj usr.sbin/kvm_mkdb/obj \
       usr.sbin/ldapctl/obj usr.sbin/ldapd/obj usr.sbin/ldomctl/obj \
       usr.sbin/ldomd/obj usr.sbin/ldpctl/obj usr.sbin/ldpd/obj \
       usr.sbin/lpd/obj \
       usr.sbin/lpr/filters/obj usr.sbin/lpr/lpc/obj usr.sbin/lpr/lpd/obj \
       usr.sbin/lpr/lpq/obj usr.sbin/lpr/lprm/obj usr.sbin/lpr/lpr/obj \
       usr.sbin/mailwrapper/obj usr.sbin/makefs/obj usr.sbin/map-mbone/obj \
       usr.sbin/memconfig/obj usr.sbin/mksuncd/obj usr.sbin/mkuboot/obj \
       usr.sbin/mopd/mopa.out/obj usr.sbin/mopd/mopchk/obj \
       usr.sbin/mopd/mopd/obj usr.sbin/mopd/mopprobe/obj \
       usr.sbin/mopd/moptrace/obj \
       usr.sbin/mrinfo/obj usr.sbin/mrouted/obj usr.sbin/mtrace/obj \
       usr.sbin/mtree/obj usr.sbin/ndp/obj usr.sbin/netgroup_mkdb/obj \
       usr.sbin/npppctl/obj usr.sbin/npppd/npppd/obj usr.sbin/nsd/obj \
       usr.sbin/ntpd/obj usr.sbin/ocspcheck/obj usr.sbin/ospf6ctl/obj \
       usr.sbin/ospf6d/obj usr.sbin/ospfctl/obj usr.sbin/ospfd/obj \
       usr.sbin/pcidump/obj usr.sbin/pkg_add/obj usr.sbin/portmap/obj \
       usr.sbin/pppd/chat/obj usr.sbin/pppd/obj usr.sbin/pppd/pppstats/obj \
       usr.sbin/procmap/obj usr.sbin/pstat/obj usr.sbin/pwd_mkdb/obj \
       usr.sbin/quotaon/obj usr.sbin/quot/obj usr.sbin/ractl/obj \
       usr.sbin/radiusctl/obj \
       usr.sbin/radiusd/obj usr.sbin/radiusd/radiusd_bsdauth/obj \
       usr.sbin/radiusd/radiusd/obj usr.sbin/radiusd/radiusd_radius/obj \
       usr.sbin/rad/obj usr.sbin/rarpd/obj usr.sbin/rbootd/obj \
       usr.sbin/rcctl/obj usr.sbin/rdate/obj usr.sbin/rdsetroot/obj \
       usr.sbin/relayctl/obj usr.sbin/relayd/obj usr.sbin/repquota/obj \
       usr.sbin/ripctl/obj usr.sbin/ripd/obj usr.sbin/rmt/obj \
       usr.sbin/route6d/obj usr.sbin/rpc.bootparamd/obj \
       usr.sbin/rpc.lockd/obj usr.sbin/rpc.statd/obj \
       usr.sbin/rpki-client/obj usr.sbin/sa/obj usr.sbin/sasyncd/obj \
       usr.sbin/sensorsd/obj usr.sbin/slaacctl/obj usr.sbin/slowcgi/obj \
       usr.sbin/smtpd/mail/obj usr.sbin/smtpd/smtpctl/obj \
       usr.sbin/smtpd/smtpd/obj usr.sbin/smtpd/smtp/obj \
       usr.sbin/snmpd/obj usr.sbin/spamdb/obj usr.sbin/switchctl/obj \
       usr.sbin/switchd/obj usr.sbin/syslogc/obj usr.sbin/syslogd/obj \
       usr.sbin/sysmerge/obj usr.sbin/syspatch/obj usr.sbin/sysupgrade/obj \
       usr.sbin/tcpdrop/obj usr.sbin/tcpdump/obj usr.sbin/tftpd/obj \
       usr.sbin/tftp-proxy/obj usr.sbin/tokenadm/obj usr.sbin/tokeninit/obj \
       usr.sbin/traceroute/obj usr.sbin/trpt/obj usr.sbin/unbound/obj \
       usr.sbin/unwindctl/obj usr.sbin/usbdevs/obj usr.sbin/user/obj \
       usr.sbin/vipw/obj usr.sbin/vmctl/obj usr.sbin/vmd/obj \
       usr.sbin/watchdogd/obj usr.sbin/wsconscfg/obj usr.sbin/wsfontload/obj \
       usr.sbin/wsmoused/obj usr.sbin/ypbind/obj usr.sbin/ypldap/obj \
       usr.sbin/yppoll/obj \
       usr.sbin/ypserv/makedbm/obj usr.sbin/ypserv/mkalias/obj \
       usr.sbin/ypserv/mknetid/obj usr.sbin/ypserv/revnetgroup/obj \
       usr.sbin/ypserv/ypinit/obj usr.sbin/ypserv/yppush/obj \
       usr.sbin/ypserv/ypserv/obj usr.sbin/ypserv/ypxfr/obj \
       usr.sbin/ypserv/obj \
       usr.sbin/ypset/obj usr.sbin/zdump/obj usr.sbin/zic/obj \
       etc/etc.alpha/obj etc/etc.amd64/obj etc/etc.arm64/obj \
       etc/etc.armv7/obj etc/etc.hppa/obj etc/etc.i386/obj \
       etc/etc.landisk/obj etc/etc.loongson/obj etc/etc.luna88k/obj \
       etc/etc.macppc/obj etc/etc.octeon/obj etc/etc.powerpc64/obj \
       etc/etc.riscv64/obj etc/etc.sparc64/obj \
       share/locale/ctype/obj \
       share/man/man1/obj share/man/man3/obj \
       share/man/man4/man4.alpha/obj share/man/man4/man4.amd64/obj \
       share/man/man4/man4.arm64/obj share/man/man4/man4.armv7/obj \
       share/man/man4/man4.hppa/obj share/man/man4/man4.i386/obj \
       share/man/man4/man4.landisk/obj share/man/man4/man4.loongson/obj \
       share/man/man4/man4.luna88k/obj share/man/man4/man4.macppc/obj \
       share/man/man4/man4.octeon/obj share/man/man4/man4.powerpc64/obj \
       share/man/man4/man4.riscv64/obj share/man/man4/man4.sparc64/obj \
       share/man/man4/obj \
       share/man/man5/obj share/man/man6/obj share/man/man7/obj \
       share/man/man8/man8.alpha/obj share/man/man8/man8.amd64/obj \
       share/man/man8/man8.arm64/obj share/man/man8/man8.armv7/obj \
       share/man/man8/man8.hppa/obj share/man/man8/man8.i386/obj \
       share/man/man8/man8.landisk/obj share/man/man8/man8.loongson/obj \
       share/man/man8/man8.luna88k/obj share/man/man8/man8.macppc/obj \
       share/man/man8/man8.octeon/obj share/man/man8/man8.powerpc64/obj \
       share/man/man8/man8.riscv64/obj share/man/man8/man8.sparc64/obj \
       share/man/man8/obj \
       share/man/man9/obj \
       share/misc/pcvtfonts/obj share/termtypes/obj share/zoneinfo/obj \
       games/adventure/obj games/arithmetic/obj games/atc/obj \
       games/backgammon/backgammon/obj games/backgammon/teachgammon/obj \
       games/banner/obj games/battlestar/obj games/bcd/obj \
       games/boggle/boggle/obj games/boggle/mkdict/obj \
       games/boggle/mkindex/obj games/bs/obj games/caesar/obj \
       games/canfield/canfield/obj games/canfield/cfscores/obj \
       games/cribbage/obj games/factor/obj games/fish/obj \
       games/fortune/datfiles/obj games/fortune/fortune/obj \
       games/fortune/strfile/obj games/gomoku/obj games/grdc/obj \
       games/hack/obj games/hangman/obj games/hunt/huntd/obj \
       games/hunt/hunt/obj games/mille/obj games/monop/obj games/morse/obj \
       games/number/obj games/phantasia/obj games/pig/obj games/pom/obj \
       games/ppt/obj games/primes/obj games/quiz/obj games/rain/obj \
       games/random/obj games/robots/obj games/sail/obj games/snake/obj \
       games/tetris/obj games/trek/obj games/worm/obj games/worms/obj \
       games/wump/obj \
       gnu/lib/libclang_rt/profile/obj gnu/lib/libcompiler_rt/obj \
       gnu/lib/libcxxabi/obj gnu/lib/libcxx/obj gnu/lib/libexecinfo/obj \
       gnu/lib/libiberty/obj gnu/lib/libreadline/doc/obj \
       gnu/lib/libreadline/obj gnu/lib/libstdc++/obj \
       gnu/lib/libstdc++-v3/obj gnu/lib/libsupc++-v3/obj \
       gnu/usr.bin/binutils-2.17/obj gnu/usr.bin/binutils/obj \
       gnu/usr.bin/cc/cc1/obj gnu/usr.bin/cc/cc1plus/obj \
       gnu/usr.bin/cc/cc_int/obj gnu/usr.bin/cc/cc/obj \
       gnu/usr.bin/cc/cc_tools/obj gnu/usr.bin/cc/c++filt/obj \
       gnu/usr.bin/cc/c++/obj gnu/usr.bin/cc/collect2/obj \
       gnu/usr.bin/cc/doc/obj gnu/usr.bin/cc/gcov/obj \
       gnu/usr.bin/cc/include/obj gnu/usr.bin/cc/libcpp/obj \
       gnu/usr.bin/cc/libdecnumber/obj gnu/usr.bin/cc/libgcc/obj \
       gnu/usr.bin/cc/libgcov/obj \
       gnu/usr.bin/clang/clang/obj gnu/usr.bin/clang/clang-tblgen/obj \
       gnu/usr.bin/clang/include/clang/AST/obj \
       gnu/usr.bin/clang/include/clang/Basic/obj \
       gnu/usr.bin/clang/include/clang/Driver/obj \
       gnu/usr.bin/clang/include/clang/intrin/obj \
       gnu/usr.bin/clang/include/clang/Parse/obj \
       gnu/usr.bin/clang/include/clang/Sema/obj \
       gnu/usr.bin/clang/include/clang/Serialization/obj \
       gnu/usr.bin/clang/include/clang/StaticAnalyzer/Checkers/obj \
       gnu/usr.bin/clang/include/lldb/Commands/obj \
       gnu/usr.bin/clang/include/lldb/Core/obj \
       gnu/usr.bin/clang/include/lldb/Interpreter/obj \
       gnu/usr.bin/clang/include/lldb/Plugins/obj \
       gnu/usr.bin/clang/include/lldb/Target/obj \
       gnu/usr.bin/clang/include/lld/ELF/obj \
       gnu/usr.bin/clang/include/llvm/AArch64/obj \
       gnu/usr.bin/clang/include/llvm/AMDGPU/obj \
       gnu/usr.bin/clang/include/llvm/ARM/obj \
       gnu/usr.bin/clang/include/llvm/Config/obj \
       gnu/usr.bin/clang/include/llvm/Frontend/OpenACC/obj \
       gnu/usr.bin/clang/include/llvm/Frontend/OpenMP/obj \
       gnu/usr.bin/clang/include/llvm/IR/obj \
       gnu/usr.bin/clang/include/llvm/Mips/obj \
       gnu/usr.bin/clang/include/llvm-objcopy/obj \
       gnu/usr.bin/clang/include/llvm/PowerPC/obj \
       gnu/usr.bin/clang/include/llvm/RISCV/obj \
       gnu/usr.bin/clang/include/llvm/Sparc/obj \
       gnu/usr.bin/clang/include/llvm/Transforms/InstCombine/obj \
       gnu/usr.bin/clang/include/llvm/X86/obj \
       gnu/usr.bin/clang/libclangAnalysis/obj \
       gnu/usr.bin/clang/libclangAST/obj gnu/usr.bin/clang/libclangBasic/obj \
       gnu/usr.bin/clang/libclangCodeGen/obj \
       gnu/usr.bin/clang/libclangDriver/obj \
       gnu/usr.bin/clang/libclangEdit/obj \
       gnu/usr.bin/clang/libclangFrontend/obj \
       gnu/usr.bin/clang/libclangFrontendTool/obj \
       gnu/usr.bin/clang/libclangLex/obj gnu/usr.bin/clang/libclangParse/obj \
       gnu/usr.bin/clang/libclangRewriteFrontend/obj \
       gnu/usr.bin/clang/libclangRewrite/obj \
       gnu/usr.bin/clang/libclangSema/obj \
       gnu/usr.bin/clang/libclangSerialization/obj \
       gnu/usr.bin/clang/liblldbABI/obj gnu/usr.bin/clang/liblldbAPI/obj \
       gnu/usr.bin/clang/liblldbBreakpoint/obj \
       gnu/usr.bin/clang/liblldbCommands/obj \
       gnu/usr.bin/clang/liblldbCore/obj \
       gnu/usr.bin/clang/liblldbDataFormatters/obj \
       gnu/usr.bin/clang/liblldbExpression/obj \
       gnu/usr.bin/clang/liblldbHostCommon/obj \
       gnu/usr.bin/clang/liblldbHostHyperbolaBSD/obj \
       gnu/usr.bin/clang/liblldbHostPOSIX/obj \
       gnu/usr.bin/clang/liblldbInitialization/obj \
       gnu/usr.bin/clang/liblldbInterpreter/obj \
       gnu/usr.bin/clang/liblldbPluginArchitecture/obj \
       gnu/usr.bin/clang/liblldbPluginDisassembler/obj \
       gnu/usr.bin/clang/liblldbPluginDynamicLoader/obj \
       gnu/usr.bin/clang/liblldbPluginExpressionParser/obj \
       gnu/usr.bin/clang/liblldbPluginInstruction/obj \
       gnu/usr.bin/clang/liblldbPluginInstrumentationRuntime/obj \
       gnu/usr.bin/clang/liblldbPluginJITLoader/obj \
       gnu/usr.bin/clang/liblldbPluginLanguage/obj \
       gnu/usr.bin/clang/liblldbPluginLanguageRuntime/obj \
       gnu/usr.bin/clang/liblldbPluginMemoryHistory/obj \
       gnu/usr.bin/clang/liblldbPluginObjectContainer/obj \
       gnu/usr.bin/clang/liblldbPluginObjectFile/obj \
       gnu/usr.bin/clang/liblldbPluginPlatform/obj \
       gnu/usr.bin/clang/liblldbPluginProcess/obj \
       gnu/usr.bin/clang/liblldbPluginScriptInterpreter/obj \
       gnu/usr.bin/clang/liblldbPluginStructuredData/obj \
       gnu/usr.bin/clang/liblldbPluginSymbolFile/obj \
       gnu/usr.bin/clang/liblldbPluginSymbolVendor/obj \
       gnu/usr.bin/clang/liblldbPluginSystemRuntime/obj \
       gnu/usr.bin/clang/liblldbPluginTypeSystem/obj \
       gnu/usr.bin/clang/liblldbPluginUnwindAssembly/obj \
       gnu/usr.bin/clang/liblldbSymbol/obj \
       gnu/usr.bin/clang/liblldbTarget/obj \
       gnu/usr.bin/clang/liblldbUtility/obj \
       gnu/usr.bin/clang/liblldCommon/obj gnu/usr.bin/clang/liblldCore/obj \
       gnu/usr.bin/clang/liblldELF/obj gnu/usr.bin/clang/libLLVM/obj \
       gnu/usr.bin/clang/libLLVMSupport/obj \
       gnu/usr.bin/clang/libLLVMTableGen/obj gnu/usr.bin/clang/lldb/obj \
       gnu/usr.bin/clang/lldb-server/obj gnu/usr.bin/clang/lldb-tblgen/obj \
       gnu/usr.bin/clang/lld/obj gnu/usr.bin/clang/llvm-config/obj \
       gnu/usr.bin/clang/llvm-objcopy/obj gnu/usr.bin/clang/llvm-objdump/obj \
       gnu/usr.bin/clang/llvm-tblgen/obj gnu/usr.bin/clang/obj \
       gnu/usr.bin/cvs/obj gnu/usr.bin/cxxfilt/obj gnu/usr.bin/gcc/obj \
       gnu/usr.bin/perl/obj gnu/usr.bin/texinfo/obj \
       gnu/usr.sbin/mkhybrid/mkhybrid/obj \
       distrib/alpha/iso/obj distrib/alpha/miniroot/obj \
       distrib/amd64/iso/obj distrib/amd64/ramdiskA/obj \
       distrib/amd64/ramdisk_cd/obj \
       distrib/arm64/iso/obj distrib/arm64/ramdisk/obj \
       distrib/armv7/miniroot/am335x/obj distrib/armv7/miniroot/cubie/obj \
       distrib/armv7/miniroot/cubox/obj distrib/armv7/miniroot/nitrogen/obj \
       distrib/armv7/miniroot/panda/obj distrib/armv7/miniroot/wandboard/obj \
       distrib/armv7/ramdisk/obj \
       distrib/hppa/iso/obj distrib/hppa/ramdisk/obj \
       distrib/i386/iso/obj distrib/i386/ramdisk_cd/obj \
       distrib/i386/ramdisk/obj \
       distrib/landisk/ramdisk/obj distrib/loongson/ramdisk/obj \
       distrib/luna88k/ramdisk/obj \
       distrib/macppc/iso/obj distrib/macppc/ramdisk/obj \
       distrib/octeon/iso/obj distrib/octeon/ramdisk/obj \
       distrib/powerpc64/iso/obj distrib/powerpc64/ramdisk/obj \
       distrib/riscv64/iso/obj distrib/riscv64/ramdisk/obj \
       distrib/sparc64/iso/obj distrib/sparc64/miniroot/obj \
       distrib/sparc64/ramdiskB/obj distrib/sparc64/ramdisk/obj \
       distrib/special/arch/obj distrib/special/bioctl/obj \
       distrib/special/cat/obj distrib/special/chmod/obj \
       distrib/special/chroot/obj distrib/special/cp/obj \
       distrib/special/date/obj distrib/special/dd/obj \
       distrib/special/df/obj distrib/special/dhclient/obj \
       distrib/special/dhcpleased/obj distrib/special/disklabel/obj \
       distrib/special/dmesg/obj distrib/special/doas/obj \
       distrib/special/ed/obj distrib/special/eeprom/obj \
       distrib/special/encrypt/obj distrib/special/fdisk/obj \
       distrib/special/fsck_ext2fs/obj distrib/special/fsck_ffs/obj \
       distrib/special/fsck_msdos/obj distrib/special/fsck/obj \
       distrib/special/ftp/obj distrib/special/ftp-ssl/obj \
       distrib/special/grep/obj distrib/special/growfs/obj \
       distrib/special/gzip/obj distrib/special/hostname/obj \
       distrib/special/ifconfig/obj distrib/special/init/obj \
       distrib/special/installboot/obj distrib/special/kbd/obj \
       distrib/special/ksh/obj distrib/special/libstubs/obj \
       distrib/special/ln/obj distrib/special/ls/obj \
       distrib/special/md5/obj distrib/special/mkdir/obj \
       distrib/special/mknod/obj distrib/special/mkuboot/obj \
       distrib/special/more/obj distrib/special/mount_cd9660/obj \
       distrib/special/mount_ext2fs/obj distrib/special/mount_ffs/obj \
       distrib/special/mount_msdos/obj distrib/special/mount_nfs/obj \
       distrib/special/mount/obj distrib/special/mount_udf/obj \
       distrib/special/mt/obj distrib/special/mv/obj \
       distrib/special/newfs_ext2fs/obj distrib/special/newfs_msdos/obj \
       distrib/special/newfs/obj distrib/special/pax/obj \
       distrib/special/pdisk/obj distrib/special/ping/obj \
       distrib/special/pwd_mkdb/obj distrib/special/reboot/obj \
       distrib/special/resolvd/obj distrib/special/restore/obj \
       distrib/special/rm/obj distrib/special/route/obj \
       distrib/special/sed/obj distrib/special/signify/obj \
       distrib/special/slaacd/obj distrib/special/sleep/obj \
       distrib/special/stty/obj distrib/special/sync/obj \
       distrib/special/sysctl/obj distrib/special/tee/obj \
       distrib/special/umount/obj \
       regress/include/bitstring/obj \
       regress/lib/csu/callbacks/atexit/obj \
       regress/lib/csu/callbacks/libaa/obj \
       regress/lib/csu/callbacks/libab/obj \
       regress/lib/csu/callbacks/pthread_atfork/obj \
       regress/lib/csu/ctors/obj regress/lib/csu/dtors/obj \
       regress/lib/csu/init_priority/obj \
       regress/lib/libagentx/obj \
       regress/lib/libc/alloca/obj regress/lib/libc/arc4random-fork/obj \
       regress/lib/libc/atexit/obj regress/lib/libc/basename/obj \
       regress/lib/libc/cephes/obj regress/lib/libc/cxa-atexit/libgd1/obj \
       regress/lib/libc/cxa-atexit/libgd2/obj \
       regress/lib/libc/cxa-atexit/test1/obj \
       regress/lib/libc/db/obj regress/lib/libc/dirname/obj \
       regress/lib/libc/env/obj regress/lib/libc/explicit_bzero/obj \
       regress/lib/libc/ffs/obj regress/lib/libc/fmemopen/obj \
       regress/lib/libc/fnmatch/obj regress/lib/libc/fpclassify/obj \
       regress/lib/libc/fread/obj regress/lib/libc/gcvt/obj \
       regress/lib/libc/getaddrinfo/obj regress/lib/libc/getcap/obj \
       regress/lib/libc/getopt_long/obj regress/lib/libc/getopt/obj \
       regress/lib/libc/glob/obj regress/lib/libc/hsearch/obj \
       regress/lib/libc/ieeefp/except/obj \
       regress/lib/libc/ieeefp/infinity/obj regress/lib/libc/ieeefp/inf/obj \
       regress/lib/libc/ieeefp/round/obj \
       regress/lib/libc/ifnameindex/obj regress/lib/libc/ldexp/obj \
       regress/lib/libc/locale/check_isw/obj \
       regress/lib/libc/locale/mbrtowc/obj \
       regress/lib/libc/locale/setlocale/obj \
       regress/lib/libc/locale/uselocale/obj \
       regress/lib/libc/locale/wcrtomb/obj \
       regress/lib/libc/longjmp/obj \
       regress/lib/libc/malloc/malloc0test/obj \
       regress/lib/libc/malloc/malloc_errno/obj \
       regress/lib/libc/malloc/malloc_general/obj \
       regress/lib/libc/malloc/malloc_threaderr/obj \
       regress/lib/libc/malloc/malloc_ulimit1/obj \
       regress/lib/libc/malloc/malloc_ulimit2/obj \
       regress/lib/libc/mkstemp/obj regress/lib/libc/modf/obj \
       regress/lib/libc/netdb/obj regress/lib/libc/open_memstream/obj \
       regress/lib/libc/orientation/obj regress/lib/libc/popen/obj \
       regress/lib/libc/printf/obj regress/lib/libc/qsort/obj \
       regress/lib/libc/regex/obj \
       regress/lib/libc/setjmp-fpu/obj regress/lib/libc/_setjmp/obj \
       regress/lib/libc/setjmp/obj regress/lib/libc/setjmp-signal/obj \
       regress/lib/libc/sigsetjmp/obj regress/lib/libc/sigthr/obj \
       regress/lib/libc/sleep/obj regress/lib/libc/sprintf/obj \
       regress/lib/libc/stdio_threading/fgetln/obj \
       regress/lib/libc/stdio_threading/fgets/obj \
       regress/lib/libc/stdio_threading/fopen/obj \
       regress/lib/libc/stdio_threading/fputs/obj \
       regress/lib/libc/stdio_threading/fread/obj \
       regress/lib/libc/stdio_threading/fwrite/obj \
       regress/lib/libc/stpncpy/obj regress/lib/libc/strchr/obj \
       regress/lib/libc/strerror/obj regress/lib/libc/strlcat/obj \
       regress/lib/libc/strlcpy/obj regress/lib/libc/strnlen/obj \
       regress/lib/libc/strtod/obj regress/lib/libc/strtol/obj \
       regress/lib/libc/strtonum/obj regress/lib/libc/sys/obj \
       regress/lib/libc/telldir/obj regress/lib/libc/time/strptime/obj \
       regress/lib/libc/timingsafe/obj regress/lib/libc/uuid/obj \
       regress/lib/libc/vis/obj regress/lib/libc/wprintf/obj \
       regress/lib/libcrypto/aead/obj regress/lib/libcrypto/aeswrap/obj \
       regress/lib/libcrypto/asn1/obj regress/lib/libcrypto/base64/obj \
       regress/lib/libcrypto/bf/obj regress/lib/libcrypto/bio/obj \
       regress/lib/libcrypto/bn/addsub/obj \
       regress/lib/libcrypto/bn/general/obj \
       regress/lib/libcrypto/bn/mont/obj regress/lib/libcrypto/bn/rand/obj \
       regress/lib/libcrypto/botan/obj regress/lib/libcrypto/CA/obj \
       regress/lib/libcrypto/cast/obj regress/lib/libcrypto/certs/obj \
       regress/lib/libcrypto/chacha/obj regress/lib/libcrypto/cms/obj \
       regress/lib/libcrypto/cts128/obj regress/lib/libcrypto/curve25519/obj \
       regress/lib/libcrypto/des/obj regress/lib/libcrypto/dh/obj \
       regress/lib/libcrypto/dsa/obj regress/lib/libcrypto/ecdh/obj \
       regress/lib/libcrypto/ecdsa/obj regress/lib/libcrypto/ec/obj \
       regress/lib/libcrypto/engine/obj regress/lib/libcrypto/evp/obj \
       regress/lib/libcrypto/exp/obj regress/lib/libcrypto/free/obj \
       regress/lib/libcrypto/gcm128/obj regress/lib/libcrypto/gost/obj \
       regress/lib/libcrypto/hkdf/obj regress/lib/libcrypto/hmac/obj \
       regress/lib/libcrypto/idea/obj regress/lib/libcrypto/ige/obj \
       regress/lib/libcrypto/init/obj regress/lib/libcrypto/md4/obj \
       regress/lib/libcrypto/md5/obj regress/lib/libcrypto/pbkdf2/obj \
       regress/lib/libcrypto/pem/obj regress/lib/libcrypto/pkcs7/obj \
       regress/lib/libcrypto/poly1305/obj regress/lib/libcrypto/rand/obj \
       regress/lib/libcrypto/rc2/obj regress/lib/libcrypto/rc4/obj \
       regress/lib/libcrypto/rmd/obj regress/lib/libcrypto/rsa/obj \
       regress/lib/libcrypto/sha1/obj regress/lib/libcrypto/sha256/obj \
       regress/lib/libcrypto/sha2/obj regress/lib/libcrypto/sha512/obj \
       regress/lib/libcrypto/sm3/obj regress/lib/libcrypto/sm4/obj \
       regress/lib/libcrypto/symbols/obj regress/lib/libcrypto/utf8/obj \
       regress/lib/libcrypto/wycheproof/obj \
       regress/lib/libcrypto/x509/bettertls/obj \
       regress/lib/libcrypto/x509/obj \
       regress/lib/libedit/chared/obj regress/lib/libedit/keymacro/obj \
       regress/lib/libedit/readline/obj regress/lib/libedit/read/obj \
       regress/lib/libevent/obj \
       regress/lib/libexpat/benchmark/obj regress/lib/libexpat/entropy/obj \
       regress/lib/libexpat/runtests/obj regress/lib/libexpat/runtestspp/obj \
       regress/lib/libfuse/obj \
       regress/lib/libm/cephes/obj regress/lib/libm/exp/obj \
       regress/lib/libm/fenv/obj regress/lib/libm/floor/obj \
       regress/lib/libm/fpaccuracy/obj regress/lib/libm/lgamma/obj \
       regress/lib/libm/msun/obj regress/lib/libm/nextafter/obj \
       regress/lib/libm/rint/obj regress/lib/libm/round/obj \
       regress/lib/libm/tgamma/obj regress/lib/libm/toint/obj \
       regress/lib/libm/trivial1/obj \
       regress/lib/libpthread/barrier/obj \
       regress/lib/libpthread/blocked_shutdown/obj \
       regress/lib/libpthread/cancel2/obj regress/lib/libpthread/cancel/obj \
       regress/lib/libpthread/cancel_wait/obj \
       regress/lib/libpthread/closefrom/obj regress/lib/libpthread/close/obj \
       regress/lib/libpthread/close_race/obj regress/lib/libpthread/cwd/obj \
       regress/lib/libpthread/dlopen/obj regress/lib/libpthread/earlysig/obj \
       regress/lib/libpthread/errno/obj regress/lib/libpthread/execve/obj \
       regress/lib/libpthread/fork/obj regress/lib/libpthread/group/obj \
       regress/lib/libpthread/malloc_duel/obj \
       regress/lib/libpthread/netdb/obj regress/lib/libpthread/pcap/obj \
       regress/lib/libpthread/poll/obj \
       regress/lib/libpthread/preemption_float/obj \
       regress/lib/libpthread/preemption/obj \
       regress/lib/libpthread/pthread_atfork/obj \
       regress/lib/libpthread/pthread_cond_timedwait/obj \
       regress/lib/libpthread/pthread_create/obj \
       regress/lib/libpthread/pthread_join/obj \
       regress/lib/libpthread/pthread_kill/obj \
       regress/lib/libpthread/pthread_mutex/obj \
       regress/lib/libpthread/pthread_once/obj \
       regress/lib/libpthread/pthread_rwlock2/obj \
       regress/lib/libpthread/pthread_rwlock/obj \
       regress/lib/libpthread/pthread_specific/obj \
       regress/lib/libpthread/readdir/obj \
       regress/lib/libpthread/restart/accept/obj \
       regress/lib/libpthread/restart/connect/obj \
       regress/lib/libpthread/restart/kevent/obj \
       regress/lib/libpthread/restart/read/obj \
       regress/lib/libpthread/restart/readv/obj \
       regress/lib/libpthread/restart/recvfrom/obj \
       regress/lib/libpthread/restart/recvmsg/obj \
       regress/lib/libpthread/select/obj \
       regress/lib/libpthread/semaphore/sem_destroy/obj \
       regress/lib/libpthread/semaphore/sem_getvalue/obj \
       regress/lib/libpthread/semaphore/sem_timedwait/obj \
       regress/lib/libpthread/semaphore/sem_trywait/obj \
       regress/lib/libpthread/semaphore/sem_wait/obj \
       regress/lib/libpthread/setjmp/obj \
       regress/lib/libpthread/setsockopt/1/obj \
       regress/lib/libpthread/setsockopt/2/obj \
       regress/lib/libpthread/setsockopt/3a/obj \
       regress/lib/libpthread/setsockopt/3/obj \
       regress/lib/libpthread/sigdeliver/obj \
       regress/lib/libpthread/siginfo/obj \
       regress/lib/libpthread/siginterrupt/obj \
       regress/lib/libpthread/signal/obj \
       regress/lib/libpthread/signals/ignore_sigchild/obj \
       regress/lib/libpthread/signals/pthread_join/obj \
       regress/lib/libpthread/signals/pthread_mutex_lock/obj \
       regress/lib/libpthread/signodefer/obj \
       regress/lib/libpthread/sigsuspend/obj \
       regress/lib/libpthread/sigwait/obj regress/lib/libpthread/sleep/obj \
       regress/lib/libpthread/socket/1/obj \
       regress/lib/libpthread/socket/2a/obj \
       regress/lib/libpthread/socket/2/obj \
       regress/lib/libpthread/socket/3/obj \
       regress/lib/libpthread/spinlock/obj regress/lib/libpthread/stack/obj \
       regress/lib/libpthread/stdarg/obj regress/lib/libpthread/stdio/obj \
       regress/lib/libpthread/switch/obj regress/lib/libpthread/system/obj \
       regress/lib/libradius/obj regress/lib/libskey/obj \
       regress/lib/libsndio/cap/obj regress/lib/libsndio/fd/obj \
       regress/lib/libsndio/play/obj regress/lib/libsndio/rec/obj \
       regress/lib/libsndio/vol/obj \
       regress/lib/libssl/asn1/obj regress/lib/libssl/buffer/obj \
       regress/lib/libssl/bytestring/obj regress/lib/libssl/ciphers/obj \
       regress/lib/libssl/client/obj regress/lib/libssl/dtls/obj \
       regress/lib/libssl/handshake/obj \
       regress/lib/libssl/interop/botan/obj \
       regress/lib/libssl/interop/cert/obj \
       regress/lib/libssl/interop/cipher/obj \
       regress/lib/libssl/interop/libressl/obj \
       regress/lib/libssl/interop/netcat/obj \
       regress/lib/libssl/interop/openssl11/obj \
       regress/lib/libssl/interop/openssl/obj \
       regress/lib/libssl/interop/session/obj \
       regress/lib/libssl/interop/version/obj \
       regress/lib/libssl/key_schedule/obj \
       regress/lib/libssl/openssl-ruby/obj regress/lib/libssl/pqueue/obj \
       regress/lib/libssl/record_layer/obj regress/lib/libssl/record/obj \
       regress/lib/libssl/server/obj regress/lib/libssl/ssl/obj \
       regress/lib/libssl/tlsext/obj regress/lib/libssl/tlsfuzzer/obj \
       regress/lib/libssl/tlslegacy/obj regress/lib/libssl/unit/obj \
       regress/lib/libssl/verify/obj \
       regress/lib/libtls/config/obj regress/lib/libtls/gotls/obj \
       regress/lib/libtls/keypair/obj regress/lib/libtls/tls/obj \
       regress/lib/libtls/verify/obj \
       regress/lib/libusbhid/usage/obj \
       regress/lib/libutil/bcrypt_pbkdf/obj regress/lib/libutil/ber/obj \
       regress/lib/libutil/fmt_scaled/obj \
       regress/lib/libutil/pkcs5_pbkdf2/obj \
       regress/libexec/ftpd/obj \
       regress/libexec/ld.so/constructor/libaa/obj \
       regress/libexec/ld.so/constructor/libab/obj \
       regress/libexec/ld.so/constructor/prog1/obj \
       regress/libexec/ld.so/constructor/prog2/obj \
       regress/libexec/ld.so/dependencies/order1/libaa/obj \
       regress/libexec/ld.so/dependencies/order1/libbb/obj \
       regress/libexec/ld.so/dependencies/order1/prog1/obj \
       regress/libexec/ld.so/df_1_noopen/obj \
       regress/libexec/ld.so/dlclose/test1/libaa/obj \
       regress/libexec/ld.so/dlclose/test1/libbb/obj \
       regress/libexec/ld.so/dlclose/test1/prog1/obj \
       regress/libexec/ld.so/dlclose/test1/prog2/obj \
       regress/libexec/ld.so/dlclose/test1/prog3/obj \
       regress/libexec/ld.so/dlclose/test2/libaa/obj \
       regress/libexec/ld.so/dlclose/test2/libbb/obj \
       regress/libexec/ld.so/dlclose/test2/libcc/obj \
       regress/libexec/ld.so/dlclose/test2/libzz/obj \
       regress/libexec/ld.so/dlclose/test2/prog1/obj \
       regress/libexec/ld.so/dlopen/libaa/obj \
       regress/libexec/ld.so/dlopen/libab/obj \
       regress/libexec/ld.so/dlopen/libac/obj \
       regress/libexec/ld.so/dlopen/prog1/obj \
       regress/libexec/ld.so/dlopen/prog2/obj \
       regress/libexec/ld.so/dlopen/prog3/obj \
       regress/libexec/ld.so/dlopen/prog4/obj \
       regress/libexec/ld.so/dlsym/test1/libaa/obj \
       regress/libexec/ld.so/dlsym/test1/prog1/obj \
       regress/libexec/ld.so/dlsym/test1/prog2/obj \
       regress/libexec/ld.so/dlsym/test2/libaa/obj \
       regress/libexec/ld.so/dlsym/test2/libbb/obj \
       regress/libexec/ld.so/dlsym/test2/libcc/obj \
       regress/libexec/ld.so/dlsym/test2/prog1/obj \
       regress/libexec/ld.so/dlsym/test2/prog2/obj \
       regress/libexec/ld.so/dlsym/test2/prog3/obj \
       regress/libexec/ld.so/dlsym/test2/prog4/obj \
       regress/libexec/ld.so/dlsym/test2/prog5/obj \
       regress/libexec/ld.so/dlsym/test3/libaa/obj \
       regress/libexec/ld.so/dlsym/test3/libbb/obj \
       regress/libexec/ld.so/dlsym/test3/libcc/obj \
       regress/libexec/ld.so/dlsym/test3/libdd/obj \
       regress/libexec/ld.so/dlsym/test3/libee/obj \
       regress/libexec/ld.so/dlsym/test3/prog1/obj \
       regress/libexec/ld.so/dlsym/test3/prog2/obj \
       regress/libexec/ld.so/dlsym/test3/prog3/obj \
       regress/libexec/ld.so/dlsym/test3/prog4/obj \
       regress/libexec/ld.so/edgecases/test1/libaa_b/obj \
       regress/libexec/ld.so/edgecases/test1/libaa_g/obj \
       regress/libexec/ld.so/edgecases/test1/prog1/obj \
       regress/libexec/ld.so/edgecases/test2/libaa/obj \
       regress/libexec/ld.so/edgecases/test2/prog1/obj \
       regress/libexec/ld.so/edgecases/test3/libaa/obj \
       regress/libexec/ld.so/edgecases/test3/libbb/obj \
       regress/libexec/ld.so/edgecases/test3/prog1/obj \
       regress/libexec/ld.so/elf/foo/obj \
       regress/libexec/ld.so/elf/libbar/obj \
       regress/libexec/ld.so/hidden/libaa/obj \
       regress/libexec/ld.so/hidden/libab/obj \
       regress/libexec/ld.so/hidden/test1/obj \
       regress/libexec/ld.so/hidden/test2/obj \
       regress/libexec/ld.so/init-env/libaa/obj \
       regress/libexec/ld.so/init-env/prog/obj \
       regress/libexec/ld.so/initfirst/test1/libif1/obj \
       regress/libexec/ld.so/initfirst/test1/libif2/obj \
       regress/libexec/ld.so/initfirst/test1/libif3/obj \
       regress/libexec/ld.so/initfirst/test1/libnormal/obj \
       regress/libexec/ld.so/initfirst/test1/prog1/obj \
       regress/libexec/ld.so/initfirst/test2/libaa/obj \
       regress/libexec/ld.so/initfirst/test2/libab/obj \
       regress/libexec/ld.so/initfirst/test2/libac/obj \
       regress/libexec/ld.so/initfirst/test2/libad/obj \
       regress/libexec/ld.so/initfirst/test2/libae/obj \
       regress/libexec/ld.so/initfirst/test2/prog1/obj \
       regress/libexec/ld.so/initfirst/test2/prog2/obj \
       regress/libexec/ld.so/lazy/libbar/obj \
       regress/libexec/ld.so/lazy/libfoo/obj \
       regress/libexec/ld.so/lazy/prog/obj \
       regress/libexec/ld.so/link-order/lib10/obj \
       regress/libexec/ld.so/link-order/lib11/obj \
       regress/libexec/ld.so/link-order/lib20/obj \
       regress/libexec/ld.so/link-order/libnover/obj \
       regress/libexec/ld.so/link-order/prog/obj \
       regress/libexec/ld.so/link-order/test/obj \
       regress/libexec/ld.so/nodelete/liba/obj \
       regress/libexec/ld.so/nodelete/test1/obj \
       regress/libexec/ld.so/randomdata/ld.so-cookie/obj \
       regress/libexec/ld.so/randomdata/libaa/obj \
       regress/libexec/ld.so/randomdata/prog-dynamic/obj \
       regress/libexec/ld.so/randomdata/prog-pie/obj \
       regress/libexec/ld.so/randomdata/prog-static/obj \
       regress/libexec/ld.so/subst/libaa/obj \
       regress/libexec/ld.so/subst/prog1/obj \
       regress/libexec/ld.so/weak/libstrong/obj \
       regress/libexec/ld.so/weak/libweak/obj \
       regress/libexec/ld.so/weak/prog2/obj \
       regress/libexec/ld.so/weak/prog/obj \
       regress/bin/cat/obj regress/bin/chmod/obj regress/bin/csh/obj \
       regress/bin/ed/obj regress/bin/expr/obj regress/bin/ksh/edit/obj \
       regress/bin/ksh/main/obj regress/bin/ln/obj regress/bin/md5/obj \
       regress/bin/pax/obj regress/bin/ps/obj regress/bin/test/obj \
       regress/sbin/disklabel/obj regress/sbin/ifconfig/obj \
       regress/sbin/iked/dh/obj regress/sbin/iked/live/obj \
       regress/sbin/iked/parser/obj regress/sbin/iked/test_helper/obj \
       regress/sbin/ipsecctl/obj \
       regress/sbin/isakmpd/crypto/obj regress/sbin/isakmpd/dh/obj \
       regress/sbin/isakmpd/hmac/obj regress/sbin/isakmpd/prf/obj \
       regress/sbin/isakmpd/util/obj \
       regress/sbin/newfs/obj regress/sbin/pfctl/obj regress/sbin/route/obj \
       regress/sbin/slaacd/obj \
       regress/usr.bin/apply/obj regress/usr.bin/basename/obj \
       regress/usr.bin/bc/obj regress/usr.bin/calendar/obj \
       regress/usr.bin/cap_mkdb/obj regress/usr.bin/colrm/obj \
       regress/usr.bin/column/obj regress/usr.bin/cut/obj \
       regress/usr.bin/dc/obj regress/usr.bin/diff3/obj \
       regress/usr.bin/diff/obj regress/usr.bin/dirname/obj \
       regress/usr.bin/doas/obj regress/usr.bin/file/obj \
       regress/usr.bin/fmt/obj regress/usr.bin/fold/obj \
       regress/usr.bin/grep/obj regress/usr.bin/gzip/obj \
       regress/usr.bin/join/obj regress/usr.bin/jot/obj \
       regress/usr.bin/lastcomm/obj regress/usr.bin/libtool/obj \
       regress/usr.bin/m4/obj regress/usr.bin/mail/obj \
       regress/usr.bin/make/obj \
       regress/usr.bin/mandoc/char/accent/obj \
       regress/usr.bin/mandoc/char/bar/obj \
       regress/usr.bin/mandoc/char/hyphen/obj \
       regress/usr.bin/mandoc/char/N/obj \
       regress/usr.bin/mandoc/char/space/obj \
       regress/usr.bin/mandoc/char/unicode/obj \
       regress/usr.bin/mandoc/db/binedit/obj \
       regress/usr.bin/mandoc/db/dbm_dump/obj \
       regress/usr.bin/mandoc/db/makeinodes/obj \
       regress/usr.bin/mandoc/db/run/obj \
       regress/usr.bin/mandoc/eqn/define/obj \
       regress/usr.bin/mandoc/eqn/delim/obj \
       regress/usr.bin/mandoc/eqn/fromto/obj \
       regress/usr.bin/mandoc/eqn/matrix/obj \
       regress/usr.bin/mandoc/eqn/nullary/obj \
       regress/usr.bin/mandoc/eqn/over/obj \
       regress/usr.bin/mandoc/eqn/size/obj \
       regress/usr.bin/mandoc/eqn/subsup/obj \
       regress/usr.bin/mandoc/eqn/unary/obj \
       regress/usr.bin/mandoc/man/BI/obj \
       regress/usr.bin/mandoc/man/blank/obj regress/usr.bin/mandoc/man/B/obj \
       regress/usr.bin/mandoc/man/EX/obj regress/usr.bin/mandoc/man/HP/obj \
       regress/usr.bin/mandoc/man/IP/obj regress/usr.bin/mandoc/man/MT/obj \
       regress/usr.bin/mandoc/man/nf/obj regress/usr.bin/mandoc/man/OP/obj \
       regress/usr.bin/mandoc/man/PD/obj regress/usr.bin/mandoc/man/PP/obj \
       regress/usr.bin/mandoc/man/RS/obj regress/usr.bin/mandoc/man/SH/obj \
       regress/usr.bin/mandoc/man/SS/obj regress/usr.bin/mandoc/man/SY/obj \
       regress/usr.bin/mandoc/man/TH/obj regress/usr.bin/mandoc/man/TP/obj \
       regress/usr.bin/mandoc/man/TS/obj regress/usr.bin/mandoc/man/UR/obj \
       regress/usr.bin/mandoc/mdoc/Ad/obj regress/usr.bin/mandoc/mdoc/An/obj \
       regress/usr.bin/mandoc/mdoc/Ap/obj regress/usr.bin/mandoc/mdoc/Aq/obj \
       regress/usr.bin/mandoc/mdoc/Ar/obj regress/usr.bin/mandoc/mdoc/At/obj \
       regress/usr.bin/mandoc/mdoc/Bd/obj regress/usr.bin/mandoc/mdoc/Bf/obj \
       regress/usr.bin/mandoc/mdoc/Bk/obj \
       regress/usr.bin/mandoc/mdoc/blank/obj \
       regress/usr.bin/mandoc/mdoc/Bl/obj \
       regress/usr.bin/mandoc/mdoc/break/obj \
       regress/usr.bin/mandoc/mdoc/Brq/obj \
       regress/usr.bin/mandoc/mdoc/Bx/obj regress/usr.bin/mandoc/mdoc/Cd/obj \
       regress/usr.bin/mandoc/mdoc/Cm/obj regress/usr.bin/mandoc/mdoc/D1/obj \
       regress/usr.bin/mandoc/mdoc/Db/obj regress/usr.bin/mandoc/mdoc/Dd/obj \
       regress/usr.bin/mandoc/mdoc/Dl/obj regress/usr.bin/mandoc/mdoc/Dq/obj \
       regress/usr.bin/mandoc/mdoc/Dt/obj regress/usr.bin/mandoc/mdoc/Dv/obj \
       regress/usr.bin/mandoc/mdoc/Em/obj regress/usr.bin/mandoc/mdoc/Eo/obj \
       regress/usr.bin/mandoc/mdoc/Er/obj regress/usr.bin/mandoc/mdoc/Ev/obj \
       regress/usr.bin/mandoc/mdoc/Ex/obj regress/usr.bin/mandoc/mdoc/Fd/obj \
       regress/usr.bin/mandoc/mdoc/Fl/obj regress/usr.bin/mandoc/mdoc/Fo/obj \
       regress/usr.bin/mandoc/mdoc/Ft/obj regress/usr.bin/mandoc/mdoc/Ic/obj \
       regress/usr.bin/mandoc/mdoc/In/obj regress/usr.bin/mandoc/mdoc/Lb/obj \
       regress/usr.bin/mandoc/mdoc/Li/obj regress/usr.bin/mandoc/mdoc/Lk/obj \
       regress/usr.bin/mandoc/mdoc/Ms/obj regress/usr.bin/mandoc/mdoc/Mt/obj \
       regress/usr.bin/mandoc/mdoc/Nd/obj regress/usr.bin/mandoc/mdoc/Nm/obj \
       regress/usr.bin/mandoc/mdoc/No/obj regress/usr.bin/mandoc/mdoc/Ns/obj \
       regress/usr.bin/mandoc/mdoc/Oo/obj regress/usr.bin/mandoc/mdoc/Op/obj \
       regress/usr.bin/mandoc/mdoc/Os/obj regress/usr.bin/mandoc/mdoc/Ox/obj \
       regress/usr.bin/mandoc/mdoc/Pa/obj regress/usr.bin/mandoc/mdoc/Pf/obj \
       regress/usr.bin/mandoc/mdoc/Pp/obj regress/usr.bin/mandoc/mdoc/Qq/obj \
       regress/usr.bin/mandoc/mdoc/Rs/obj regress/usr.bin/mandoc/mdoc/Rv/obj \
       regress/usr.bin/mandoc/mdoc/Sh/obj regress/usr.bin/mandoc/mdoc/Sm/obj \
       regress/usr.bin/mandoc/mdoc/Sq/obj regress/usr.bin/mandoc/mdoc/St/obj \
       regress/usr.bin/mandoc/mdoc/Sx/obj regress/usr.bin/mandoc/mdoc/Sy/obj \
       regress/usr.bin/mandoc/mdoc/Tg/obj regress/usr.bin/mandoc/mdoc/Tn/obj \
       regress/usr.bin/mandoc/mdoc/Ud/obj regress/usr.bin/mandoc/mdoc/Ux/obj \
       regress/usr.bin/mandoc/mdoc/Va/obj regress/usr.bin/mandoc/mdoc/Vt/obj \
       regress/usr.bin/mandoc/mdoc/Xr/obj \
       regress/usr.bin/mandoc/roff/args/obj \
       regress/usr.bin/mandoc/roff/br/obj regress/usr.bin/mandoc/roff/cc/obj \
       regress/usr.bin/mandoc/roff/ce/obj \
       regress/usr.bin/mandoc/roff/char/obj \
       regress/usr.bin/mandoc/roff/cond/obj \
       regress/usr.bin/mandoc/roff/de/obj regress/usr.bin/mandoc/roff/ds/obj \
       regress/usr.bin/mandoc/roff/esc/obj \
       regress/usr.bin/mandoc/roff/ft/obj regress/usr.bin/mandoc/roff/ig/obj \
       regress/usr.bin/mandoc/roff/it/obj regress/usr.bin/mandoc/roff/ll/obj \
       regress/usr.bin/mandoc/roff/na/obj regress/usr.bin/mandoc/roff/nr/obj \
       regress/usr.bin/mandoc/roff/po/obj regress/usr.bin/mandoc/roff/ps/obj \
       regress/usr.bin/mandoc/roff/return/obj \
       regress/usr.bin/mandoc/roff/rm/obj regress/usr.bin/mandoc/roff/rn/obj \
       regress/usr.bin/mandoc/roff/scale/obj \
       regress/usr.bin/mandoc/roff/shift/obj \
       regress/usr.bin/mandoc/roff/sp/obj \
       regress/usr.bin/mandoc/roff/string/obj \
       regress/usr.bin/mandoc/roff/ta/obj regress/usr.bin/mandoc/roff/ti/obj \
       regress/usr.bin/mandoc/roff/tr/obj \
       regress/usr.bin/mandoc/roff/while/obj \
       regress/usr.bin/mandoc/tbl/data/obj \
       regress/usr.bin/mandoc/tbl/layout/obj \
       regress/usr.bin/mandoc/tbl/macro/obj \
       regress/usr.bin/mandoc/tbl/mod/obj regress/usr.bin/mandoc/tbl/opt/obj \
       regress/usr.bin/nc/obj \
       regress/usr.bin/openssl/obj regress/usr.bin/openssl/options/obj \
       regress/usr.bin/openssl/x509/obj \
       regress/usr.bin/patch/obj regress/usr.bin/pkg-config/obj \
       regress/usr.bin/rcs/obj regress/usr.bin/rev/obj \
       regress/usr.bin/sdiff/obj regress/usr.bin/sed/obj \
       regress/usr.bin/signify/obj regress/usr.bin/snmp/obj \
       regress/usr.bin/sort/obj \
       regress/usr.bin/ssh/misc/sk-dummy/obj regress/usr.bin/ssh/obj \
       regress/usr.bin/tsort/obj regress/usr.bin/ul/obj \
       regress/usr.bin/wc/obj regress/usr.bin/xargs/obj \
       regress/usr.sbin/acme-client/obj regress/usr.sbin/arp/obj \
       regress/usr.sbin/bgpd/config/obj \
       regress/usr.sbin/bgpd/integrationtests/obj \
       regress/usr.sbin/bgpd/unittests/obj \
       regress/usr.sbin/httpd/patterns/obj regress/usr.sbin/httpd/tests/obj \
       regress/usr.sbin/ifstated/obj regress/usr.sbin/ldapd/obj \
       regress/usr.sbin/mtree/obj regress/usr.sbin/ocspcheck/obj \
       regress/usr.sbin/ospf6d/obj regress/usr.sbin/ospfd/obj \
       regress/usr.sbin/pkg_add/obj regress/usr.sbin/relayd/obj \
       regress/usr.sbin/rpki-client/libressl/obj regress/usr.sbin/snmpd/obj \
       regress/usr.sbin/switchd/obj regress/usr.sbin/syslogd/obj \
       regress/misc/c++abi/obj \
       regress/misc/exceptions/foo/obj regress/misc/exceptions/libbar/obj \
       regress/misc/exceptions/simple2/obj \
       regress/misc/exceptions/simple/obj \
       regress/misc/exceptions/threads/obj \
       regress/misc/os-test/obj regress/misc/posixtestsuite/obj \
       regress/misc/X11/blt/obj regress/misc/X11/bltone/obj
