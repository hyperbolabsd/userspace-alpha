# $OpenBSD: Makefile,v 1.20 2017/07/08 20:38:31 krw Exp $
#
# Copyright (c) 1996, 1997 The Internet Software Consortium.
# All rights reserved.
#
# Modifications to support HyperbolaBSD:
# Copyright (c) 2023 Hyperbola Project
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of The Internet Software Consortium nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE INTERNET SOFTWARE CONSORTIUM AND
# CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
# BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
# THE INTERNET SOFTWARE CONSORTIUM OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
#

.include <bsd.own.mk>

SRCS=	dhclient.c clparse.c dispatch.c bpf.c options.c \
	conflex.c log.c packet.c  \
	parse.c privsep.c kroute.c

PROG=	dhclient
#LDADD+=	-lutil
#DPADD+= ${LIBUTIL}
MAN=	dhclient.8 dhclient.conf.5 dhclient.leases.5

CFLAGS+=-Wall
CFLAGS+=-Wstrict-prototypes -Wmissing-prototypes
CFLAGS+=-Wmissing-declarations
CFLAGS+=-Wshadow -Wpointer-arith -Wcast-qual
CFLAGS+=-Wsign-compare

# Use system headers from source
CFLAGS+=-I${.CURDIR}/../../build/include

# Dependency libraries
#LDADD+=${.CURDIR}/../../lib/libc/obj/libc.a
LDADD+=${.CURDIR}/../../lib/libc/obj/libc.so.96.1
#LDADD+=${.CURDIR}/../../lib/libutil/obj/libutil.a
LDADD+=${.CURDIR}/../../lib/libutil/obj/libutil.so.15.1

# Set "ld.so" path
LDADD+=-Wl,-dynamic-linker,/libexec/ld.so

# Fix this warning: cannot find entry symbol _start
LDADD+=${.CURDIR}/../../lib/csu/obj/crt0.o
LDADD+=${.CURDIR}/../../lib/csu/obj/crtbegin.o

# Disable static build
STATIC=

.include <bsd.prog.mk>
