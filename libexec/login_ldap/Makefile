# $Id: Makefile,v 1.1 2020/09/12 15:06:12 martijn Exp $

PROG=		login_ldap
SRCS=		aldap.c util.c search.c bind.c
SRCS+=		login_ldap.c
MAN=		login_ldap.8

CFLAGS+=	-Wall -I${.CURDIR}
CFLAGS+=	-Wstrict-prototypes -Wmissing-prototypes
CFLAGS+=	-Wmissing-declarations
CFLAGS+=	-Wshadow -Wpointer-arith -Wcast-qual
CFLAGS+=	-Wsign-compare

# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * CFLAGS
# * DPADD
# * LDADD
#
# Start of modifications made by Hyperbola Project
#LDADD+=		-levent -ltls -lssl -lcrypto -lutil
#DPADD+=		${LIBEVENT} ${LIBTLS} ${LIBSSL} ${LIBCRYPTO} ${LIBUTIL}

# Use system headers from source
CFLAGS+=-I${.CURDIR}/../../build/include

# Dependency libraries
#LDADD+=${.CURDIR}/../../lib/libc/obj/libc.a
LDADD+=${.CURDIR}/../../lib/libc/obj/libc.so.96.1
#LDADD+=${.CURDIR}/../../lib/libcrypto/obj/libcrypto.a
LDADD+=${.CURDIR}/../../lib/libcrypto/obj/libcrypto.so.47.0
#LDADD+=${.CURDIR}/../../lib/libevent/obj/libevent.a
LDADD+=${.CURDIR}/../../lib/libevent/obj/libevent.so.4.1
#LDADD+=${.CURDIR}/../../lib/libssl/obj/libssl.a
LDADD+=${.CURDIR}/../../lib/libssl/obj/libssl.so.50.0
#LDADD+=${.CURDIR}/../../lib/libtls/obj/libtls.a
LDADD+=${.CURDIR}/../../lib/libtls/obj/libtls.so.22.0
#LDADD+=${.CURDIR}/../../lib/libutil/obj/libutil.a
LDADD+=${.CURDIR}/../../lib/libutil/obj/libutil.so.15.1

# Set "ld.so" path
LDADD+=-Wl,-dynamic-linker,/libexec/ld.so

# Fix this warning: cannot find entry symbol _start
LDADD+=${.CURDIR}/../../lib/csu/obj/crt0.o
LDADD+=${.CURDIR}/../../lib/csu/obj/crtbegin.o

# End of modifications made by Hyperbola Project

BINDIR=		/usr/libexec/auth

.include <bsd.prog.mk>
