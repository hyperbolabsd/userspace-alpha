#	$OpenBSD: Makefile.inc,v 1.7 2019/10/20 03:44:49 guenther Exp $

# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * CFLAGS
#
# Start of modifications made by Hyperbola Project
CFLAGS += -fPIC -mno-sse2 -mno-sse -mno-3dnow -mno-mmx -mfpmath=387
# End of modifications made by Hyperbola Project

AFLAGS += -fpic
LD_SCRIPT = ${.CURDIR}/${MACHINE_CPU}/ld.script

# Suppress DWARF2 warnings
DEBUG?= -gdwarf-4

RELATIVE_RELOC=R_X86_64_RELATIVE
