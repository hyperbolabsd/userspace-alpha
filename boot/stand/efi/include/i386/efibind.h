/*
 * Public Domain.
 *
 * Modifications to support HyperbolaBSD:
 * Written in 2022-2023 by Hyperbola Project
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <sys/stdint.h>

typedef int8_t INT8;
typedef uint8_t UINT8;
typedef int16_t INT16;
typedef uint16_t UINT16;
typedef int32_t INT32;
typedef uint32_t UINT32;

/* This architecture does not have RSP (64bit) register used by asm() in efiboot.c */
//typedef ALIGNAS_K(8) int64_t INT64;
//typedef ALIGNAS_K(8) uint64_t UINT64;
typedef ALIGNAS_K(4) int32_t INT64;
typedef ALIGNAS_K(4) uint32_t UINT64;

typedef void VOID;

typedef int32_t INTN;
typedef uint32_t UINTN;

#define INTERFACE_DECL(x) struct x
#define EFIAPI

#define EFIERR(x) (0x80000000 | x)
