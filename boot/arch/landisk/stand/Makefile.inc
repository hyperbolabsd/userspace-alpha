#	$OpenBSD: Makefile.inc,v 1.4 2017/07/25 13:32:14 robert Exp $

.if !make(clean) && !make(cleandir) && !make(includes) && !make(libdep) && \
    !make(sadep) && !make(salibdir) && !make(obj)
.BEGIN:
	@([ X$(S) = X -o -h machine ] || ln -s $(S)/arch/landisk/include machine)
	@([ X$(S) = X -o -h sh ] || ln -s $(S)/arch/sh/include sh)
.endif

# Modifications to support HyperbolaBSD:
# Written in 2023-2024 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * CFLAGS
#
# Start of modifications made by Hyperbola Project
CFLAGS+=-Os -fno-expensive-optimizations
CFLAGS+=-fno-strict-aliasing -fno-strict-overflow -fno-tree-vrp
CFLAGS+=-fno-stack-protector -fno-builtin
# End of modifications made by Hyperbola Project

CLEANFILES+=	machine sh
BINDIR=	/usr/mdec
MANSUBDIR=landisk
