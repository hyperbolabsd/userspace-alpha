#	$OpenBSD: Makefile.inc,v 1.18 2017/07/25 13:32:14 robert Exp $

# Modifications to support HyperbolaBSD:
# Written in 2023-2024 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * CFLAGS
#
# Start of modifications made by Hyperbola Project
CFLAGS=${CCOMPFLAGS} ${DEBUG} ${COPTS} -Os -fno-expensive-optimizations
CFLAGS+=-fno-strict-aliasing -fno-strict-overflow -fno-tree-vrp
CFLAGS+=-Wall -Werror
# End of modifications made by Hyperbola Project

CFLAGS+=	-ffreestanding -fno-stack-protector -DMDRANDOM
CPPFLAGS+=-I${S} -I${SADIR}/libsa -I. -I${.CURDIR}
SACFLAGS=-D_STANDALONE
DEBUGFLAGS=
# DEBUGFLAGS+=-DDEBUG
# DEBUGFLAGS+=-DGIDT_DEBUG
# DEBUGFLAGS+=-DBIOS_DEBUG
# DEBUGFLAGS+=-DEXEC_DEBUG
# DEBUGFLAGS+=-DALLOC_TRACE
# DEBUGFLAGS+=-DUNIX_DEBUG
# DEBUGFLAGS+=-DBOOTP_DEBUG -DNETIF_DEBUG -DETHER_DEBUG
# DEBUGFLAGS+=-DNFS_DEBUG -DRPC_DEBUG -DRARP_DEBUG

# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * LINKADDR
#
# Start of modifications made by Hyperbola Project
#LINKADDR=0x40120
LINKADDR=0x40060
# End of modifications made by Hyperbola Project

LOADADDR=0x40000
HEAP_LIMIT=0xA0000
BOOTREL=0x60000
BOOTMAGIC=0xc001d00d
#ROM_SIZE=32768

# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * CLEANFILES
#
# Start of modifications made by Hyperbola Project
CLEANFILES+=	assym.h machine sys dev ufs crypto isofs net netinet netinet6 nfs
# End of modifications made by Hyperbola Project

SACFLAGS+=-nostdinc -fno-builtin -fpack-struct

.include <bsd.own.mk>
.if ${COMPILER_VERSION:Mclang}
NO_INTEGR_AS=	-no-integrated-as
.endif


# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * genassym.sh path
# * sys symbolic link
#
# Start of modifications made by Hyperbola Project
.if !make(clean) && !make(cleandir) && !make(includes) && !make(libdep) && \
    !make(sadep) && !make(salibdir) && !make(obj)
.BEGIN:
	@([ X$(S) = X -o -h machine ] || ln -s $(S)/../sys/arch/amd64/include machine)
	@([ X$(S) = X -o -h sys ] || ln -s $(S)/../sys/sys sys)
	@([ X$(S) = X -o -h dev ] || ln -s $(S)/../sys/dev dev)
	@([ X$(S) = X -o -h ufs ] || ln -s $(S)/../sys/ufs ufs)
	@([ X$(S) = X -o -h crypto ] || ln -s $(S)/../sys/crypto crypto)
	@([ X$(S) = X -o -h isofs ] || ln -s $(S)/../sys/isofs isofs)
	@([ X$(S) = X -o -h net ] || ln -s $(S)/../sys/net net)
	@([ X$(S) = X -o -h netinet ] || ln -s $(S)/../sys/netinet netinet)
	@([ X$(S) = X -o -h netinet6 ] || ln -s $(S)/../sys/netinet6 netinet6)
	@([ X$(S) = X -o -h nfs ] || ln -s $(S)/../sys/nfs nfs)

assym.h: ${S}/../sys/kern/genassym.sh ${SADIR}/etc/genassym.cf
	sh ${S}/../sys/kern/genassym.sh ${CC} ${NO_INTEGR_AS} ${CFLAGS} ${CPPFLAGS} \
	    ${PARAM} < ${SADIR}/etc/genassym.cf > assym.h.tmp && \
	    mv -f assym.h.tmp assym.h
.endif
# End of modifications made by Hyperbola Project

# NO_NET=no_net
BINDIR=	/usr/mdec

MANSUBDIR=amd64
