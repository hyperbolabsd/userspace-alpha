
<img alt="HYPERBOLABSD_LOGOTYPE_IMAGE" src="https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/logotypes/logotype_symbol_and_text-g_hyperbola-n_hyperbolabsd_symbol_and_text-i0-u_sldstrPblr_sldshpPblr_grdshdPblr_grdlgtPblr-c_hyperbola_and_dark_text-r8910x2048px-a0f1urwgothic_s0-t_svg1d1basic.svg" text="HyperbolaBSD™ logotype image" width="512">
<img alt="HYPER_BOLA_CHARACTER_IMAGE" src="https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/drawings/drawing_character-g_hyperbola-n_hyperbola-p_standing_in_bipedal_and_leaning_his_right_hand-i1-u_sldstr_sldshp_sldshdPblr_sldlgtPblr-c_hypercolour-r2048px2-a0f0s0-t_svg1d1basic.svg" text="Hyper Bola🄯 character image" height="160">

HyperbolaBSD&trade; userspace
=============================

The __data structures and algorithms__ used by
[Hyperbola Project&trade;][HYPERBOLA]<br/>
in __HyperbolaBSD&trade; userspace__ are all either taken<br/>
from the [HyperbolaBSD&trade; algorithms][ALGORITHMS] to the general public<br/>
or are inventions of [Hyperbola Project&trade;][HYPERBOLA].

Getting the source code and building HyperbolaBSD&trade; userspace
------------------------------------------------------------------

The following requirements are needed:

*   __GCC__ and __Binutils__ in cross compiling <br/>
    as __arch-machine-hyperbolabsd__ triplet <br/>
    __(example: x86_64-unknown-hyperbolabsd)__
*   __BSD make__, __rpcsvc-proto__, __Sharutils__ and __Universal Ctags__.
*   __OpenBSD lorder__ (use __usr.bin/lorder/lorder.sh__), <br/>
    due the BSD lorder from __bsdmainutils__ is works different.

The next preparation steps are:

*   Install __GCC__ and __Binutils__ for HyperbolaBSD cross compilation.

    `$ TARCH=x86_64-unknown-hyperbolabsd`<br/>
    `$ doas pacman -S $TARCH-gcc $TARCH-binutils`

*   Install __BSD make__, __rpcsvc-proto__, __Sharutils__
    and __Universal Ctags__.

    `$ doas pacman -S bmake rpcsvc-proto sharutils ctags`

*   Clone this repository:

    `$ URL=https://git.hyperbola.info:50100`<br/>
    `$ git clone $URL/hyperbolabsd/userspace-alpha.git`<br/>
    `$ unset URL`

*   Install __OpenBSD lorder__.

    `$ cd userspace-alpha`<br/>
    `$ doas cp usr.bin/lorder/lorder.sh /usr/local/bin/lorder`<br/>
    `$ doas chown 0:0 /usr/local/bin/lorder`<br/>
    `$ doas chmod 0755 /usr/local/bin/lorder`

*   Prepare the __HyperBK&trade;__ source code:

    `$ ./sys.sh`

*   Generate __obj__ directories:

    `$ bmake -f obj.mk`

*   Make the _HyperbolaBSD_ kernel _("bmake install" to install this kernel)_:

    `$ NTHREADS=2`<br/>
    `$ bmake -m $PWD/share/mk -j $NTHREADS CC=$TARCH-cc LD=$TARCH-ld`<br/>
    `$ unset CC LD NTHREADS TARCH`

Steps to clean source code:

    `$ cd userspace-alpha`<br/>
    `$ bmake -m $PWD/share/mk clean`<br/>
    `$ bmake -f obj.mk clean`

---
>   Notes:

>   *   Works with __AMD64 (x86_64)__ only.
>   *   Some programs does not compile:
>       *   __sbin/unwind__
>       *   __usr.bin/make__
>       *   __usr.bin/nm__
>       *   __usr.bin/unifdef__
>       *   __usr.bin/vi__
>       *   __usr.sbin/nsd__
>       *   __usr.sbin/pkg_add__
>       *   __usr.sbin/switchd__
>       *   __usr.sbin/unbound__
>       *   __gnu__
>       *   __games__
---

License:
--------

*   __README__
    by _[M&aacute;rcio Silva][COADDE]_ is under the terms of either:<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_.


Licenses for _logotype_ and _character_:
----------------------------------------

*   [__HyperbolaBSD&trade;__ logotype image _2017/05/12_][LOGOTYPE]<br/>
    by _[Andr&eacute; Silva][EMULATORMAN]_, _[M&aacute;rcio Silva][COADDE]_ and
    _[Jos&eacute; Silva][CRAZYTOON]_ is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.

*   [__Hyper Bola&#127279;__ character image _2017/04_, _2019/01/22_ and
    _2019/07/10_][CHARACTER]<br/>
    by _[Jos&eacute; Silva][CRAZYTOON]_, _[M&aacute;rcio Silva][COADDE]_ and
    _[Andr&eacute; Silva][EMULATORMAN]_ is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.


[LOGOTYPE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/documents/information_character-g_hyperbola-n_hyperbola-t_md.md
    "HyperbolaBSD™ logotype image"
[CHARACTER]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/drawings/drawing_character-g_hyperbola-n_hyperbola-p_standing_in_bipedal_and_leaning_his_right_hand-i1-u_sldstr_sldshp-c_hypercolour-r2048px2-a0f0s0-t_svg1d2tiny.svg
    "Hyper Bola🄯 character image"

[COADDE]: coadde@hyperbola.info
    "Márcio Silva (Co.Ad.De.) <coadde@hyperbola.info>"
[CRAZYTOON]: crazytoon@hyperbola.info
    "José Silva (Crazytoon) <crazytoon@hyperbola.info>"
[EMULATORMAN]: emulatorman@hyperbola.info
    "André Silva (Emulatorman) <emulatorman@hyperbola.info>"

[GPL]: COPYING
    "GNU® General Public License version 3"
[LICENSES]: LICENSES
    "Compatible licenses with GNU® General Public License version 3 (GPLv3)"
[FAL]: https://artlibre.org/licence/lal/en/
    "Free Art License version 1.3"
[CCBYSA]: https://creativecommons.org/licenses/by-sa/4.0/
    "Creative Commons® Attribution-ShareAlike 4.0 International License"
[FDL+SE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/COPYING_FDL_P_SE_V1_3
    "GNU® Free Documentation License version 1.3 with the special exceptions"
[HYPERBOLA]: https://www.hyperbola.info/about/
    "Hyperbola Project"
[ALGORITHMS]: https://git.hyperbola.info:50100/hyperbolabsd/algorithms.git/
    "HyperbolaBSD algorithms"
