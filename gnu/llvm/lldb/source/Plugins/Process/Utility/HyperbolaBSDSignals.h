//===-- OpenBSDSignals.h ----------------------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef liblldb_HyperbolaBSDSignals_H_
#define liblldb_HyperbolaBSDSignals_H_

#include "lldb/Target/UnixSignals.h"

namespace lldb_private {

class HyperbolaBSDSignals : public UnixSignals {
public:
  HyperbolaBSDSignals();

private:
  void Reset() override;
};

} // namespace lldb_private

#endif // liblldb_HyperbolaBSDSignals_H_
