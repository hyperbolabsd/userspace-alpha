//===-- OpenBSDSignals.cpp --------------------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include "HyperbolaBSDSignals.h"

using namespace lldb_private;

HyperbolaBSDSignals::HyperbolaBSDSignals() : UnixSignals() { Reset(); }

void HyperbolaBSDSignals::Reset() {
  UnixSignals::Reset();

  //        SIGNO   NAME            SUPPRESS STOP   NOTIFY DESCRIPTION
  //        ======  ============    ======== ====== ======
  //        ===================================================
  AddSignal(32, "SIGTHR", false, false, false, "thread library AST");
}
