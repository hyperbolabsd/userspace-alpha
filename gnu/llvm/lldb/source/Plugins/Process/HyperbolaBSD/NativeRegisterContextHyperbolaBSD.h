//===-- NativeRegisterContextOpenBSD.h ---------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef lldb_NativeRegisterContextHyperbolaBSD_h
#define lldb_NativeRegisterContextHyperbolaBSD_h

#include "lldb/Host/common/NativeThreadProtocol.h"

#include "Plugins/Process/HyperbolaBSD/NativeProcessHyperbolaBSD.h"
#include "Plugins/Process/Utility/NativeRegisterContextRegisterInfo.h"

namespace lldb_private {
namespace process_hyperbolabsd {

class NativeRegisterContextHyperbolaBSD : public NativeRegisterContextRegisterInfo {
public:
  NativeRegisterContextHyperbolaBSD(NativeThreadProtocol &native_thread,
                              RegisterInfoInterface *reg_info_interface_p);

  // This function is implemented in the NativeRegisterContextHyperbolaBSD_*
  // subclasses to create a new instance of the host specific
  // NativeRegisterContextHyperbolaBSD. The implementations can't collide as only one
  // NativeRegisterContextHyperbolaBSD_* variant should be compiled into the final
  // executable.
  static std::unique_ptr<NativeRegisterContextHyperbolaBSD>
  CreateHostNativeRegisterContextHyperbolaBSD(const ArchSpec &target_arch,
                                        NativeThreadProtocol &native_thread);

protected:
  virtual Status ReadGPR();
  virtual Status WriteGPR();

  virtual Status ReadFPR();
  virtual Status WriteFPR();

  virtual void *GetGPRBuffer() { return nullptr; }
  virtual size_t GetGPRSize() {
    return GetRegisterInfoInterface().GetGPRSize();
  }

  virtual void *GetFPRBuffer() { return nullptr; }
  virtual size_t GetFPRSize() { return 0; }

  virtual Status DoReadGPR(void *buf);
  virtual Status DoWriteGPR(void *buf);

  virtual Status DoReadFPR(void *buf);
  virtual Status DoWriteFPR(void *buf);

  virtual NativeProcessHyperbolaBSD &GetProcess();
  virtual ::pid_t GetProcessPid();
};

} // namespace process_hyperbolabsd
} // namespace lldb_private

#endif // #ifndef lldb_NativeRegisterContextHyperbolaBSD_h
