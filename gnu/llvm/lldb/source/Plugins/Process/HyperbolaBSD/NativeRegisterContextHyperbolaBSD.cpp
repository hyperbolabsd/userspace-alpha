//===-- NativeRegisterContextOpenBSD.cpp -------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include "NativeRegisterContextHyperbolaBSD.h"

#include "lldb/Host/common/NativeProcessProtocol.h"

using namespace lldb_private;
using namespace lldb_private::process_hyperbolabsd;

// clang-format off
#include <sys/types.h>
#include <sys/ptrace.h>
// clang-format on

NativeRegisterContextHyperbolaBSD::NativeRegisterContextHyperbolaBSD(
    NativeThreadProtocol &native_thread,
    RegisterInfoInterface *reg_info_interface_p)
    : NativeRegisterContextRegisterInfo(native_thread,
                                        reg_info_interface_p) {}

Status NativeRegisterContextHyperbolaBSD::ReadGPR() {
  void *buf = GetGPRBuffer();
  if (!buf)
    return Status("GPR buffer is NULL");

  return DoReadGPR(buf);
}

Status NativeRegisterContextHyperbolaBSD::WriteGPR() {
  void *buf = GetGPRBuffer();
  if (!buf)
    return Status("GPR buffer is NULL");

  return DoWriteGPR(buf);
}

Status NativeRegisterContextHyperbolaBSD::ReadFPR() {
  void *buf = GetFPRBuffer();
  if (!buf)
    return Status("FPR buffer is NULL");

  return DoReadFPR(buf);
}

Status NativeRegisterContextHyperbolaBSD::WriteFPR() {
  void *buf = GetFPRBuffer();
  if (!buf)
    return Status("FPR buffer is NULL");

  return DoWriteFPR(buf);
}

Status NativeRegisterContextHyperbolaBSD::DoReadGPR(void *buf) {
#ifdef PT_GETREGS
  return NativeProcessHyperbolaBSD::PtraceWrapper(PT_GETREGS, GetProcessPid(), buf,
                                            m_thread.GetID());
#else
  return Status("PT_GETREGS not supported on this platform");
#endif
}

Status NativeRegisterContextHyperbolaBSD::DoWriteGPR(void *buf) {
#ifdef PT_SETREGS
  return NativeProcessHyperbolaBSD::PtraceWrapper(PT_SETREGS, GetProcessPid(), buf,
                                            m_thread.GetID());
#else
  return Status("PT_SETREGS not supported on this platform");
#endif
}

Status NativeRegisterContextHyperbolaBSD::DoReadFPR(void *buf) {
#ifdef PT_GETFPREGS
  return NativeProcessHyperbolaBSD::PtraceWrapper(PT_GETFPREGS, GetProcessPid(), buf,
                                            m_thread.GetID());
#else
  return Status("PT_GETFPREGS not supported on this platform");
#endif
}

Status NativeRegisterContextHyperbolaBSD::DoWriteFPR(void *buf) {
#ifdef PT_SETFPREGS
  return NativeProcessHyperbolaBSD::PtraceWrapper(PT_SETFPREGS, GetProcessPid(), buf,
                                            m_thread.GetID());
#else
  return Status("PT_SETFPREGS not supported on this platform");
#endif
}

NativeProcessHyperbolaBSD &NativeRegisterContextHyperbolaBSD::GetProcess() {
  return static_cast<NativeProcessHyperbolaBSD &>(m_thread.GetProcess());
}

::pid_t NativeRegisterContextHyperbolaBSD::GetProcessPid() {
  return GetProcess().GetID();
}
