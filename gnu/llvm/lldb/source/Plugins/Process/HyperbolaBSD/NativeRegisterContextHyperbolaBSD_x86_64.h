//===-- NativeRegisterContextOpenBSD_x86_64.h --------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef lldb_NativeRegisterContextHyperbolaBSD_x86_64_h
#define lldb_NativeRegisterContextHyperbolaBSD_x86_64_h

// clang-format off
#include <sys/types.h>
#include <machine/reg.h>
// clang-format on

#include "Plugins/Process/HyperbolaBSD/NativeRegisterContextHyperbolaBSD.h"
#include "Plugins/Process/Utility/RegisterContext_x86.h"
#include "Plugins/Process/Utility/lldb-x86-register-enums.h"

namespace lldb_private {
namespace process_hyperbolabsd {

class NativeProcessHyperbolaBSD;

class NativeRegisterContextHyperbolaBSD_x86_64 : public NativeRegisterContextHyperbolaBSD {
public:
  NativeRegisterContextHyperbolaBSD_x86_64(const ArchSpec &target_arch,
                                     NativeThreadProtocol &native_thread);
  size_t GetGPRSize() override { return sizeof(m_gpr); }
  size_t GetFPRSize() override { return sizeof(m_fpr); }

  uint32_t GetUserRegisterCount() const override;
  uint32_t GetRegisterSetCount() const override;

  const RegisterSet *GetRegisterSet(uint32_t set_index) const override;

  Status ReadRegister(const RegisterInfo *reg_info,
                      RegisterValue &reg_value) override;

  Status WriteRegister(const RegisterInfo *reg_info,
                       const RegisterValue &reg_value) override;

  Status ReadAllRegisterValues(lldb::DataBufferSP &data_sp) override;

  Status WriteAllRegisterValues(const lldb::DataBufferSP &data_sp) override;

protected:
  void *GetGPRBuffer() override { return &m_gpr; }
  void *GetFPRBuffer() override { return &m_fpr; }

private:
  // Private member types.
  enum { GPRegSet, FPRegSet };

  // Private member variables.
  struct reg m_gpr;
  struct fpreg m_fpr;

  int GetSetForNativeRegNum(int reg_num) const;

  int ReadRegisterSet(uint32_t set);
  int WriteRegisterSet(uint32_t set);
};

} // namespace process_hyperbolabsd
} // namespace lldb_private

#endif // #ifndef lldb_NativeRegisterContextHyperbolaBSD_x86_64_h
