# If you change this file, please alsolook at files which source this one:
# shlelf_hbsd.sh

. ${srcdir}/emulparams/shelf.sh
. ${srcdir}/emulparams/elf_hbsd.sh

OUTPUT_FORMAT="elf32-sh-hbsd"
TEXT_START_ADDR=0x400000
MAXPAGESIZE=0x10000

DATA_START_SYMBOLS='__data_start = . ;';

ENTRY=__start

unset EMBEDDED
unset OTHER_SECTIONS
