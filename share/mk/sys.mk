#	$OpenBSD: sys.mk,v 1.90 2021/08/17 15:03:56 deraadt Exp $
#	$NetBSD: sys.mk,v 1.27 1996/04/10 05:47:19 mycroft Exp $
#	@(#)sys.mk	5.11 (Berkeley) 3/13/91

.if defined(EXTRA_SYS_MK_INCLUDES)
.for __SYS_MK_INCLUDE in ${EXTRA_SYS_MK_INCLUDES}
.include ${__SYS_MK_INCLUDE}
.endfor
.endif

# Modifications to support HyperbolaBSD:
# Written in 2023-2024 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * unix
# * AFLAGS
# * CFLAGS
# * CPPFLAGS
# * CXXFLAGS
# * LDFLAGS
# * OSMAJOR
# * OSMINOR
# * COMPILER_VERSION
# * MACHINE
# * MACHINE_ARCH
# * MACHINE_CPU
# * TSORT
# * TSORT_QUIET
# * UNAME_ARCH
# * UNAME_MACH
# * UNAME_CPU
# * -fno-expensive-optimizations
# * LIBCRT0
# * CRTBEGIN
# * CRTEND
#
# Start of modifications made by Hyperbola Project
unix=		We run HyperbolaBSD.
OSMAJOR=	0
OSMINOR=	99
OSREV=		$(OSMAJOR).$(OSMINOR)
OSrev=		$(OSMAJOR)$(OSMINOR)

# Do not use system headers and libraries
CPPFLAGS+=	-nostdinc
CXXFLAGS+=	-nostdinc++
LDFLAGS+=	-nodefaultlibs
LDFLAGS+=	-nostartfiles
LDFLAGS+=	-nostdlib

# Add build-id
CFLAGS+=	-Wl,-build-id=sha1

.SUFFIXES: .out .a .o .c .cc .C .cxx .cpp .F .f .r .y .l .s .S .cl .p .h .sh .m4

UNAME_MACH!=	uname -p
UNAME_ARCH!=	uname -m

COMPILER_VERSION?=gcc4

# alpha amd64 arm arm64 armv7 hppa i386 landisk loongson luna88k m88k macppc
# mips64 octeon powerpc powerpc64 riscv64 sh sparc64
.if ${UNAME_MACH} == "unknown"
MACHINE?=	${UNAME_ARCH}
.else
MACHINE?=	${UNAME_MACH}
.endif

.if ${UNAME_ARCH} == "alpha" || ${UNAME_ARCH} == "amd64" \
    || ${UNAME_ARCH} == "arm" || ${UNAME_ARCH} == "arm64" \
    || ${UNAME_ARCH} == "hppa" || ${UNAME_ARCH} == "i386" \
    || ${UNAME_ARCH} == "m88k" || ${UNAME_ARCH} == "mips64" \
    || ${UNAME_ARCH} == "powerpc" || ${UNAME_ARCH} == "powerpc64" \
    || ${UNAME_ARCH} == "riscv64" || ${UNAME_ARCH} == "sh" \
    || ${UNAME_ARCH} == "sparc64"
MACHINE_ARCH=	${UNAME_ARCH}
MACHINE_CPU=	${UNAME_ARCH}
.elif ${UNAME_ARCH} == "aarch64" || ${UNAME_ARCH} == "aarch64eb" \
    || ${UNAME_ARCH} == "arm64eb"
MACHINE_ARCH?=	arm64
MACHINE_CPU?=	arm64
.elif ${UNAME_ARCH} == "armeb" \
    || ${UNAME_ARCH} == "armv6l" || ${UNAME_ARCH} == "armv6l_eb" \
    || ${UNAME_ARCH} == "armv7l" || ${UNAME_ARCH} == "armv7l_eb"
MACHINE_ARCH?=	arm
MACHINE_CPU?=	arm
.elif ${UNAME_ARCH} == "hppa64" \
    || ${UNAME_ARCH} == "parisc" || ${UNAME_ARCH} == "parisc64"
MACHINE_ARCH?=	hppa
MACHINE_CPU?=	hppa
.elif ${UNAME_ARCH} == "i486" || ${UNAME_ARCH} == "i586" \
    || ${UNAME_ARCH} == "i686" || ${UNAME_ARCH} == "ia32" \
    || ${UNAME_ARCH} == "x86" || ${UNAME_ARCH} == "x86_32"
MACHINE_ARCH?=	i386
MACHINE_CPU?=	i386
.elif ${UNAME_ARCH} == "mips"
MACHINE_ARCH?=	${UNAME_ARCH}"64"
MACHINE_CPU?=	${UNAME_ARCH}"64"
.elif ${UNAME_ARCH} == "mips64el"
MACHINE_ARCH?=	mips64
MACHINE_CPU?=	${UNAME_ARCH}
.elif ${UNAME_ARCH} == "mipsel"
MACHINE_ARCH?=	mips64
MACHINE_CPU?=	mips64el
.elif ${UNAME_ARCH} == "powerpc64el"
MACHINE_ARCH?=	powerpc64
MACHINE_CPU?=	${UNAME_ARCH}
.elif ${UNAME_ARCH} == "ppc"
MACHINE_ARCH?=	powerpc
MACHINE_CPU?=	powerpc
.elif ${UNAME_ARCH} == "ppc64"
MACHINE_ARCH?=	powerpc64
MACHINE_CPU?=	powerpc64
.elif ${UNAME_ARCH} == "ppc64el"
MACHINE_ARCH?=	powerpc64
MACHINE_CPU?=	powerpc64el
.elif ${UNAME_ARCH} == "sparc"
MACHINE_ARCH?=	${UNAME_ARCH}"64"
MACHINE_CPU?=	${UNAME_ARCH}"64"
.elif ${UNAME_ARCH} == "x86_64"
MACHINE_ARCH=	amd64
MACHINE_CPU=	amd64
.endif

.if ${MACHINE_ARCH} == "alpha"
AFLAGS+=	-mno-soft-float -mfp-reg -mieee
CFLAGS+=	-mno-soft-float -mfp-reg -mieee
CXXFLAGS+=	-mno-soft-float -mfp-reg -mieee
.elif ${MACHINE_ARCH} == "amd64"
AFLAGS+=	-march=x86-64 -mtune=generic -mhard-float -msse -msse2 -mieee-fp -mno-fancy-math-387 -mfpmath=sse
CFLAGS+=	-march=x86-64 -mtune=generic -mhard-float -msse -msse2 -mieee-fp -mno-fancy-math-387 -mfpmath=sse
CXXFLAGS+=	-march=x86-64 -mtune=generic -mhard-float -msse -msse2 -mieee-fp -mno-fancy-math-387 -mfpmath=sse
.elif ${MACHINE_ARCH} == "arm"
AFLAGS+=	-mfloat-abi=hard
CFLAGS+=	-mfloat-abi=hard
CXXFLAGS+=	-mfloat-abi=hard
.elif ${MACHINE_ARCH} == "i386"
.if ${UNAME_ARCH} == "i586"
AFLAGS+=	-march=i586 -mtune=generic -mhard-float -mmmx -mieee-fp -mno-fancy-math-387
CFLAGS+=	-march=i586 -mtune=generic -mhard-float -mmmx -mieee-fp -mno-fancy-math-387
CXXFLAGS+=	-march=i586 -mtune=generic -mhard-float -mmmx -mieee-fp -mno-fancy-math-387
.elif ${UNAME_ARCH} == "i686"
AFLAGS+=	-march=i686 -mtune=generic -mhard-float -msse -mieee-fp -mno-fancy-math-387
CFLAGS+=	-march=i686 -mtune=generic -mhard-float -msse -mieee-fp -mno-fancy-math-387
CXXFLAGS+=	-march=i686 -mtune=generic -mhard-float -msse -mieee-fp -mno-fancy-math-387
.else
AFLAGS+=	-march=i386 -mtune=intel -mhard-float -mieee-fp -mno-fancy-math-387
CFLAGS+=	-march=i386 -mtune=intel -mhard-float -mieee-fp -mno-fancy-math-387
CXXFLAGS+=	-march=i386 -mtune=intel -mhard-float -mieee-fp -mno-fancy-math-387
.endif
.elif ${MACHINE_ARCH} == "mips64" ||${MACHINE_ARCH} == "powerpc" || ${MACHINE_ARCH} == "powerpc64"
AFLAGS+=	-mhard-float
CFLAGS+=	-mhard-float
CXXFLAGS+=	-mhard-float
.elif ${MACHINE_ARCH} == "riscv64"
AFLAGS+=	-march=rv64g -mtune=sifive-7-series -mabi=lp64 -mfdiv
CFLAGS+=	-march=rv64g -mtune=sifive-7-series -mabi=lp64 -mfdiv
CXXFLAGS+=	-march=rv64g -mtune=sifive-7-series -mabi=lp64 -mfdiv
.elif ${MACHINE_ARCH} == "sh"
AFLAGS+=	-mieee
CFLAGS+=	-mieee
CXXFLAGS+=	-mieee
.elif ${MACHINE_ARCH} == "sparc64"
AFLAGS+=	-mcpu=v9 -mtune=ultrasparc -mhard-float -mhard-quad-float -mfmaf -mfsmuld
CFLAGS+=	-mcpu=v9 -mtune=ultrasparc -mhard-float -mhard-quad-float -mfmaf -mfsmuld
CXXFLAGS+=	-mcpu=v9 -mtune=ultrasparc -mhard-float -mhard-quad-float -mfmaf -mfsmuld
.endif
MACHINE=	${MACHINE_ARCH}

AFLAGS+=	-std=gnu17
CFLAGS+=	-std=gnu17
CXXFLAGS+=	-std=gnu17

AR?=		ar
ARFLAGS?=	r
RANLIB?=	ranlib
LORDER?=	lorder
TSORT?=		tsort
TSORT_QUIET?=	${TSORT} 2>/dev/null

AS?=		as
AFLAGS?=	${DEBUG}
COMPILE.s?=	${CC} ${AFLAGS} -c
LINK.s?=	${CC} ${AFLAGS} ${LDFLAGS}
COMPILE.S?=	${CC} ${AFLAGS} ${CPPFLAGS} -c
LINK.S?=	${CC} ${AFLAGS} ${CPPFLAGS} ${LDFLAGS}

CC?=		cc

PIPE?=		-pipe

CFLAGS+=	-O2 -fno-expensive-optimizations
CFLAGS+=	-fno-strict-aliasing -fno-strict-overflow -fno-tree-vrp
CFLAGS+=	${PIPE} ${DEBUG}
COMPILE.c?=	${CC} ${CFLAGS} ${CPPFLAGS} -c
LINK.c?=	${CC} ${CFLAGS} ${CPPFLAGS} ${LDFLAGS}

HOSTCC?=	cc

CXX?=		c++
CXXFLAGS+=	-O2 -fno-expensive-optimizations
CXXFLAGS+=	-fno-strict-aliasing -fno-strict-overflow -fno-tree-vrp
CXXFLAGS+=	${PIPE} ${DEBUG}
COMPILE.cc?=	${CXX} ${CXXFLAGS} ${CPPFLAGS} -c
LINK.cc?=	${CXX} ${CXXFLAGS} ${CPPFLAGS} ${LDFLAGS}

CPP?=		cpp
CPPFLAGS?=	

FC?=		f77
FFLAGS+=	-O2 -fno-expensive-optimizations
FFLAGS+=	-fno-strict-aliasing -fno-strict-overflow -fno-tree-vrp
RFLAGS?=
COMPILE.f?=	${FC} ${FFLAGS} -c
LINK.f?=	${FC} ${FFLAGS} ${LDFLAGS}
COMPILE.F?=	${FC} ${FFLAGS} ${CPPFLAGS} -c
LINK.F?=	${FC} ${FFLAGS} ${CPPFLAGS} ${LDFLAGS}
COMPILE.r?=	${FC} ${FFLAGS} ${RFLAGS} -c
LINK.r?=	${FC} ${FFLAGS} ${RFLAGS} ${LDFLAGS}

LEX?=		lex
LFLAGS?=
LEX.l?=		${LEX} ${LFLAGS}

LD?=		ld
LDFLAGS+=	${DEBUG}

MAKE?=		make

PC?=		pc
PFLAGS?=
COMPILE.p?=	${PC} ${PFLAGS} ${CPPFLAGS} -c
LINK.p?=	${PC} ${PFLAGS} ${CPPFLAGS} ${LDFLAGS}

SHELL?=		sh

YACC?=		yacc
YFLAGS?=	-d
YACC.y?=	${YACC} ${YFLAGS}

INSTALL?=	install

CTAGS?=		/usr/bin/ctags

LIBCRT0=	${.CURDIR}/../../lib/csu/obj/crt0.o
CRTBEGIN=	${.CURDIR}/../../lib/csu/obj/crtbegin.o
CRTEND=		${.CURDIR}/../../lib/csu/obj/crtend.o
# End of modifications made by Hyperbola Project

# C
.c:
	${LINK.c} -o ${.TARGET} ${.IMPSRC} ${LDLIBS}
.c.o:
	${COMPILE.c} ${.IMPSRC}
.c.a:
	${COMPILE.c} ${.IMPSRC}
	${AR} ${ARFLAGS} $@ $*.o
	rm -f $*.o

# C++
.cc:
	${LINK.cc} -o ${.TARGET} ${.IMPSRC} ${LDLIBS}
.cc.o:
	${COMPILE.cc} ${.IMPSRC}
.cc.a:
	${COMPILE.cc} ${.IMPSRC}
	${AR} ${ARFLAGS} $@ $*.o
	rm -f $*.o

.C:
	${LINK.cc} -o ${.TARGET} ${.IMPSRC} ${LDLIBS}
.C.o:
	${COMPILE.cc} ${.IMPSRC}
.C.a:
	${COMPILE.cc} ${.IMPSRC}
	${AR} ${ARFLAGS} $@ $*.o
	rm -f $*.o

.cxx:
	${LINK.cc} -o ${.TARGET} ${.IMPSRC} ${LDLIBS}
.cxx.o:
	${COMPILE.cc} ${.IMPSRC}
.cxx.a:
	${COMPILE.cc} ${.IMPSRC}
	${AR} ${ARFLAGS} $@ $*.o
	rm -f $*.o

.cpp:
	${LINK.cc} -o ${.TARGET} ${.IMPSRC} ${LDLIBS}
.cpp.o:
	${COMPILE.cc} ${.IMPSRC}
.cpp.a:
	${COMPILE.cc} ${.IMPSRC}
	${AR} ${ARFLAGS} $@ $*.o
	rm -f $*.o

# Fortran/Ratfor
.f:
	${LINK.f} -o ${.TARGET} ${.IMPSRC} ${LDLIBS}
.f.o:
	${COMPILE.f} ${.IMPSRC}
.f.a:
	${COMPILE.f} ${.IMPSRC}
	${AR} ${ARFLAGS} $@ $*.o
	rm -f $*.o

.F:
	${LINK.F} -o ${.TARGET} ${.IMPSRC} ${LDLIBS}
.F.o:
	${COMPILE.F} ${.IMPSRC}
.F.a:
	${COMPILE.F} ${.IMPSRC}
	${AR} ${ARFLAGS} $@ $*.o
	rm -f $*.o

.r:
	${LINK.r} -o ${.TARGET} ${.IMPSRC} ${LDLIBS}
.r.o:
	${COMPILE.r} ${.IMPSRC}
.r.a:
	${COMPILE.r} ${.IMPSRC}
	${AR} ${ARFLAGS} $@ $*.o
	rm -f $*.o

# Pascal
.p:
	${LINK.p} -o ${.TARGET} ${.IMPSRC} ${LDLIBS}
.p.o:
	${COMPILE.p} ${.IMPSRC}
.p.a:
	${COMPILE.p} ${.IMPSRC}
	${AR} ${ARFLAGS} $@ $*.o
	rm -f $*.o

# Assembly
.s:
	${LINK.s} -o ${.TARGET} ${.IMPSRC} ${LDLIBS}
.s.o:
	${COMPILE.s} ${.IMPSRC}
.s.a:
	${COMPILE.s} ${.IMPSRC}
	${AR} ${ARFLAGS} $@ $*.o
	rm -f $*.o
.S:
	${LINK.S} -o ${.TARGET} ${.IMPSRC} ${LDLIBS}
.S.o:
	${COMPILE.S} ${.IMPSRC}
.S.a:
	${COMPILE.S} ${.IMPSRC}
	${AR} ${ARFLAGS} $@ $*.o
	rm -f $*.o

# Lex
.l:
	${LEX.l} -o lex.${.PREFIX}.c ${.IMPSRC}
	${LINK.c} -o ${.TARGET} lex.${.PREFIX}.c ${LDLIBS} -ll
	rm -f lex.${.PREFIX}.c
.l.c:
	${LEX.l} -o ${.TARGET} ${.IMPSRC}
.l.o:
	${LEX.l} -o lex.${.PREFIX}.c ${.IMPSRC}
	${COMPILE.c} -o ${.TARGET} lex.${.PREFIX}.c
	rm -f lex.${.PREFIX}.c
	if test -f ${.TARGET:R}.d; then sed -i -e 's,lex.${.PREFIX}.c,${.IMPSRC},' ${.TARGET:R}.d; fi

# Yacc
.y:
	${YACC.y} ${.IMPSRC}
	${LINK.c} -o ${.TARGET} y.tab.c ${LDLIBS}
	rm -f y.tab.c
.y.c:
	${YACC.y} ${.IMPSRC}
	mv y.tab.c ${.TARGET}
.y.o:
	${YACC.y} ${.IMPSRC}
	${COMPILE.c} -o ${.TARGET} y.tab.c
	rm -f y.tab.c
	if test -f ${.TARGET:R}.d; then sed -i -e 's,y.tab.c,${.IMPSRC},' ${.TARGET:R}.d; fi

# Shell
.sh:
	rm -f ${.TARGET}
	cp ${.IMPSRC} ${.TARGET}
