# $OpenBSD: Makefile,v 1.8 2017/07/06 12:15:23 espie Exp $

PROG=		switchd
MAN=		switchd.8 switchd.conf.5

SRCS=		imsg_util.c log.c packet.c proc.c switch.c timer.c util.c
SRCS+=		switchd.c control.c ofp.c ofp10.c ofp13.c ofp_common.c ofrelay.c
SRCS+=		${.OBJDIR}/ofp_map.c ${.OBJDIR}/ofp10_map.c
SRCS+=		parse.y ofcconn.c

# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * CFLAGS
# * DPADD
# * LDADD
# * STATIC
#
# Start of modifications made by Hyperbola Project
#LDADD=		-levent -lutil
#DPADD=		${LIBEVENT} ${LIBUTIL}

# Use system headers from source
CFLAGS+=-I${.CURDIR}/../../build/include

# Dependency libraries
#LDADD+=${.CURDIR}/../../lib/libc/obj/libc.a
LDADD+=${.CURDIR}/../../lib/libc/obj/libc.so.96.1
#LDADD+=${.CURDIR}/../../lib/libevent/obj/libevent.a
#LDADD+=${.CURDIR}/../../lib/libevent/obj/libevent.so.4.1
#LDADD+=${.CURDIR}/../../lib/libutil/obj/libutil.a
#LDADD+=${.CURDIR}/../../lib/libutil/obj/libutil.so.15.1

# Set "ld.so" path
LDADD+=-Wl,-dynamic-linker,/libexec/ld.so

# Fix this warning: cannot find entry symbol _start
LDADD+=${.CURDIR}/../../lib/csu/obj/crt0.o
LDADD+=${.CURDIR}/../../lib/csu/obj/crtbegin.o

# Disable static build
STATIC=

# End of modifications made by Hyperbola Project

CFLAGS+=	-Wall -I${.CURDIR} -I${.CURDIR}/../switchd
CFLAGS+=	-Wstrict-prototypes -Wmissing-prototypes
CFLAGS+=	-Wmissing-declarations
CFLAGS+=	-Wshadow -Wpointer-arith -Wcast-qual
CFLAGS+=	-Wsign-compare

GENERATED=	ofp_map.c ofp10_map.c
CLEANFILES+=	${GENERATED}
YFLAGS=

ofp_map.c: genmap.sh ${.CURDIR}/ofp_map.h ${.CURDIR}/../../sys/net/ofp.h
	/bin/sh ${.CURDIR}/genmap.sh -i ${.CURDIR}/../../sys/net/ofp.h -t ofp \
	    -m ${.CURDIR}/ofp_map.h -h '<net/ofp.h>' > $@
	@touch $@

ofp10_map.c: genmap.sh ${.CURDIR}/ofp_map.h ${.CURDIR}/ofp10.h
	/bin/sh ${.CURDIR}/genmap.sh -i ${.CURDIR}/ofp10.h -t ofp10 \
	    -m ${.CURDIR}/ofp_map.h -h '"ofp10.h"' > $@
	@touch $@

.include <bsd.prog.mk>
