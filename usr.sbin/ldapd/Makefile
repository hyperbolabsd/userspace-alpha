#	$OpenBSD: Makefile,v 1.17 2020/09/19 09:46:04 tb Exp $

PROG=		ldapd
MAN=		ldapd.8 ldapd.conf.5
SRCS=		log.c logmsg.c control.c \
		util.c ldapd.c ldape.c conn.c attributes.c namespace.c \
		btree.c filter.c search.c parse.y \
		auth.c modify.c index.c evbuffer_tls.c \
		validate.c uuid.c schema.c imsgev.c syntax.c matching.c

# Modifications to support HyperbolaBSD:
# Written in 2023 by Hyperbola Project
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.
#
# Note: modifications were made in order to solve compilation error
#
# Modifications are:
# * CFLAGS
# * DPADD
# * LDADD
# * STATIC
#
# Start of modifications made by Hyperbola Project
#LDADD=		-levent -ltls -lssl -lcrypto -lz -lutil
#DPADD=		${LIBEVENT} ${LIBTLS} ${LIBSSL} ${LIBCRYPTO} ${LIBZ} ${LIBUTIL}

# Use system headers from source
CFLAGS+=-I${.CURDIR}/../../build/include

# Dependency libraries
#LDADD+=${.CURDIR}/../../lib/libc/obj/libc.a
LDADD+=${.CURDIR}/../../lib/libc/obj/libc.so.96.1
#LDADD+=${.CURDIR}/../../lib/libcrypto/obj/libcrypto.a
LDADD+=${.CURDIR}/../../lib/libcrypto/obj/libcrypto.so.47.0
#LDADD+=${.CURDIR}/../../lib/libevent/obj/libevent.a
LDADD+=${.CURDIR}/../../lib/libevent/obj/libevent.so.4.1
#LDADD+=${.CURDIR}/../../lib/libssl/obj/libssl.a
LDADD+=${.CURDIR}/../../lib/libssl/obj/libssl.so.50.0
#LDADD+=${.CURDIR}/../../lib/libtls/obj/libtls.a
LDADD+=${.CURDIR}/../../lib/libtls/obj/libtls.so.22.0
#LDADD+=${.CURDIR}/../../lib/libutil/obj/libutil.a
LDADD+=${.CURDIR}/../../lib/libutil/obj/libutil.so.15.1
#LDADD+=${.CURDIR}/../../lib/libz/obj/libz.a
LDADD+=${.CURDIR}/../../lib/libz/obj/libz.so.6.0

# Set "ld.so" path
LDADD+=-Wl,-dynamic-linker,/libexec/ld.so

# Fix this warning: cannot find entry symbol _start
LDADD+=${.CURDIR}/../../lib/csu/obj/crt0.o
LDADD+=${.CURDIR}/../../lib/csu/obj/crtbegin.o

# Disable static build
STATIC=

# End of modifications made by Hyperbola Project

CFLAGS+=	-I${.CURDIR} -g
CFLAGS+=	-Wall -Wstrict-prototypes -Wmissing-prototypes
CFLAGS+=	-Wmissing-declarations
CFLAGS+=	-Wshadow -Wpointer-arith -Wcast-qual
CFLAGS+=	-Wsign-compare
CLEANFILES+=	y.tab.h parse.c

SCHEMA_FILES=	bsd.schema \
		core.schema \
		inetorgperson.schema \
		nis.schema

distribution:
	for i in ${SCHEMA_FILES}; do \
		${INSTALL} -C -o root -g wheel -m 0644 ${.CURDIR}/schema/$$i ${DESTDIR}/etc/ldap/; \
	done

.include <bsd.prog.mk>
